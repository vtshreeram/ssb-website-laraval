function readURL(input, position, flg) {
	var inputId = input.id;
	var prevImgId = $("#" + inputId)
		.parent()
		.find("img")
		.attr("id"); //find parent img id
	strInput = inputId.substring(0, inputId.indexOf("_") + 1);
	strPrevImg = prevImgId.substring(0, prevImgId.indexOf("_") + 1);
	var imgName = $("#" + strInput + position).val();
	var ext = imgName.split(".").pop().toLowerCase();
	if ($.inArray(ext, ["gif", "png", "jpg", "jpeg", "svg"])) {

		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				if (flg == 1) {
					$("#" + strPrevImg + position).attr("src", public + "/images/pdf.png");
				}
				else {
					$("#" + strPrevImg + position).attr("src", e.target.result);
				}

				$("#" + inputId).next().val(imgName);

			};
			reader.readAsDataURL(input.files[0]);
		}
	} else {

		$("#" + strPrevImg + position).attr("src", "");

	}
}
/*
+---------------------------------------------+
	clear on set no image display
+---------------------------------------------+
*/
function clear_image(para1, img = null) {

	if (img)
		$("#" + para1).attr("src", public + "/images/" + img);
	else
		$("#" + para1).attr("src", public + "/images/no-image.png");

}
function isNumber(evt) {
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	}
	return true;
}
