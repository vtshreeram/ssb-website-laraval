<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property_flat extends Model
{
    use HasFactory;
    protected $fillable  = ['property_id','floor','sqft','bhk','status'];
}
