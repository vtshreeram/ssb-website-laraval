<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property_specification extends Model
{
    use HasFactory;
    protected $fillable = ['property_id','specification_id','name','description', 'status'];
   
    public function specification()
    {
        return $this->belongsTo(Specification::class, 'specification_id');
    }
}
