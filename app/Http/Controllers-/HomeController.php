<?php

namespace App\Http\Controllers;

use App\Http\Requests\specificationvalidate;
use App\Models\Specification;
use App\Models\Amenity;
use App\Models\City;
use App\Models\Flat;
use App\Models\Property;
use App\Models\Property_specification;
use App\Models\State;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('home');
    }
    public function specification()
    {
        $specificationArr = specification::all();
        return view('specification.index', compact('specificationArr'));
    }
    public function specificationForm()
    {
        return view('specification.create');
    }
    public function specificationCreate(Request  $request, specification $specification)
    {

        $request->validate(['name' => 'required',
                            'icon' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048']);

        $specification->name = $request->name;
        $specification->description = $request->description;
        $imageName = time().'.'.$request->icon->extension();
        $request->icon->move(public_path('images/icon'), $imageName);
        $specification->icon = $imageName;
        $specification->save();
        return redirect('admin/specification')->with('status', 'successfully addes');
    }
    public function specificationEdit(Request $request, specification $specification)
    {

        if (isPost()) {
            $request->validate(['name' => 'required',
                                'icon' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048']);
            $specification->name = $request->name;
            $specification->description = $request->description;
            if(file_exists(public_path('images/icon/'.$specification->icon))){
                unlink(public_path('images/icon/'.$specification->icon));
            }
            $imageName = time().'.'.$request->icon->extension();
            $request->icon->move(public_path('images/icon'), $imageName);
            $specification->icon = $imageName;
            $specification->save();
            return redirect('admin/specification');
        } else {
            return view('specification.edit', compact('specification'));
        }
    }
    public function getSpecification(specification $specification)
    {
        return response($specification);
    }
    public function specificationDelete(specification $specification)
    {
        $specification->delete();
        if(file_exists(public_path('images/icon/'.$specification->icon))){
        unlink(public_path('images/icon/'.$specification->icon));
        }
        return redirect('admin/specification');
    }
    //amenity
    public function amenity()
    {
        $amenityArr = Amenity::all();
        return view('amenity.index', compact('amenityArr'));
    }
    public function amenityForm()
    {
        return view('amenity.create');
    }
    public function amenityCreate(Request  $request, Amenity $amenity)
    {
        $request->validate(['name' => 'required',
                            'icon' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

        $amenity->name = $request->name;

        $imageName = time().'.'.$request->icon->extension();
        $request->icon->move(public_path('images/icon'), $imageName);
        $amenity->icon = $imageName;
        $amenity->save();
        return redirect('admin/amenity')->with('status', 'successfully addes');
    }
    public function amenityEdit(Request $request, Amenity $amenity)
    {
        if (isPost()) {
            $request->validate(['name' => 'required',
                                'icon' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048']);
            $amenity->name = $request->name;
            if(file_exists(public_path('images/icon/'.$amenity->icon))){
            unlink(public_path('images/icon/'.$amenity->icon));
            }
            $imageName = time().'.'.$request->icon->extension();
            $request->icon->move(public_path('images/icon'), $imageName);
            $amenity->icon = $imageName;

            $amenity->save();
            return redirect('admin/amenity');
        } else {
            return view('amenity.edit', compact('amenity'));
        }
    }
    public function amenityDelete(Amenity $amenity)
    {
        $amenity->delete();
        if(file_exists(public_path('images/icon/'.$amenity->icon))){
        unlink(public_path('images/icon/'.$amenity->icon));
        }
        return redirect('admin/amenity');
    }
    public function FrontHome()
    {
        $property = Property::all();
        return view('FrontHome' , compact('property'));
    }
    public function propertyDetail($id)
    {
        
        $property = Property::where("id" , $id)->with(['specification','flat'])->first();
        
        $amenity = explode(",",$property['amenity']);
        $amenityArr = [];
        foreach($amenity as $dt)
        {
            $amenityArr[] = Amenity::select('icon' ,'name')->where('id',$dt)->first()->toArray();
        }
        $property['amenity'] = $amenityArr;
        
        return view('property-detail' , compact('property'));
    }
    public function getState($id)
    {
        $state = State::where("country_id",$id)->get();
        return response($state);
    }
    public function getcity($id)
    {
        $city = City::where("state_id",$id)->get();
        return response($city);
    }
    public function PropertyGrid(Property $property)
    {
        
        $property = Property::paginate(1);
        return view('property-grid' , compact('property'));
     

        
    }
    public function propertyList()
    {
        $property = Property::all();
        return view('property-list' , compact('property'));
    }
    public function ContactUs()
    {        
        return view('contact-us');
    }
    public function jv()
    {
        return view('jv');
    }
    public function gallery()
    {
        return view('gallery');
    }
    public function aboutUs()
    { 
        return view('about-us');
    }
    public function bunglow()
    {
        return view('bunglow');
    }
}
