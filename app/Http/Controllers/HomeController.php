<?php

namespace App\Http\Controllers;

use App\Http\Requests\specificationvalidate;
use App\Models\Specification;
use App\Models\Amenity;
use App\Models\City;
use App\Models\Country;
use App\Models\Flat;
use App\Models\Property;
use App\Models\Property_specification;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Models\Property_image;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function index()
    {
       
        return view('home');
    }
    public function specification()
    {
        $specificationArr = specification::all();
        return view('specification.index', compact('specificationArr'));
    }
    public function specificationForm()
    {
        return view('specification.create');
    }
    public function specificationCreate(Request  $request, specification $specification)
    {

        $request->validate(['name' => 'required',
                            'icon' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

        $specification->name = $request->name;
        $specification->description = $request->description;
        $imageName = time().'.'.$request->icon->extension();
        $request->icon->move(public_path('images/icon'), $imageName);
        $specification->icon = $imageName;
        $specification->save();
        Session::flash('message', 'Specification Created Successfully...'); 
        Session::flash( 'class' ,'alert-success');
        return redirect('ssb-admn/specification')->with('status', 'successfully addes');
    }
    public function specificationEdit(Request $request, specification $specification)
    {
      
        if (isPost()) {
            $request->validate(['name' => 'required']);
            $specification->name = $request->name;
            $specification->description = $request->description;
            $specification->icon = $specification->icon;
            if($request->hasFile('icon')){
                unlink(public_path('images/icon/'.$specification->icon));
                $imageName = time().'.'.$request->icon->extension();
                $request->icon->move(public_path('images/icon'), $imageName);
                $specification->icon = $imageName;
            }
            $specification->save();
            Session::flash('message', 'Specification Edit Successfully...'); 
            Session::flash( 'class' ,'alert-success');
            return redirect('ssb-admn/specification');
        } else {
            return view('specification.edit', compact('specification'));
        }
    }
    public function getSpecification(specification $specification)
    {
        return response($specification);
    }
    public function specificationDelete(specification $specification)
    {
        $specification->delete();
        if(file_exists(public_path('images/icon/'.$specification->icon))){
        unlink(public_path('images/icon/'.$specification->icon));
        }
        Session::flash('message', 'Specification Deleted Successfully...'); 
        Session::flash( 'class' ,'alert-danger');
        return redirect('ssb-admn/specification');
    }
    //amenity
    public function amenity()
    {
        $amenityArr = Amenity::all();
        return view('amenity.index', compact('amenityArr'));
    }
    public function amenityForm()
    {
        return view('amenity.create');
    }
    public function amenityCreate(Request  $request, Amenity $amenity)
    {
        $request->validate(['name' => 'required',
                            'icon' => 'required|image|mimes:jpeg,png,svg,jpg,gif,svg|max:2048']);

        $amenity->name = $request->name;

        $imageName = time().'.'.$request->icon->extension();
        $request->icon->move(public_path('images/icon'), $imageName);
        $amenity->icon = $imageName;
        $amenity->save();
        Session::flash('message', 'Amenity created Successfully...'); 
        Session::flash( 'class' ,'alert-success');
        return redirect('ssb-admn/amenity')->with('status', 'successfully addes');
    }
    public function amenityEdit(Request $request, Amenity $amenity)
    {
        if (isPost()) {
            $request->validate(['name' => 'required',]);
            $amenity->name = $request->name;
            $amenity->icon = $amenity->icon;
            if($request->hasFile('icon')){
                unlink(public_path('images/icon/'.$amenity->icon));
                $imageName = time().'.'.$request->icon->extension();
                $request->icon->move(public_path('images/icon'), $imageName);
                $amenity->icon = $imageName;
            }

            $amenity->save();
            Session::flash('message', 'Amenity Edited Successfully...'); 
            Session::flash( 'class' ,'alert-success');
            return redirect('ssb-admn/amenity');
        } else {
            return view('amenity.edit', compact('amenity'));
        }
    }
    public function amenityDelete(Amenity $amenity)
    {
        $amenity->delete();
        if(file_exists(public_path('images/icon/'.$amenity->icon))){
            unlink(public_path('images/icon/'.$amenity->icon));
        }
        Session::flash( 'message' ,  'Amenity Deleted Successfully...'); 
        Session::flash( 'class' ,'alert-danger');
        return redirect('ssb-admn/amenity');
    }
    public function FrontHome()
    {
        $property = Property::where([['label', 'LIKE', '%On going%'],['label', 'not LIKE', '%Completed%']])->orderBy('short_id')->paginate(3);
        // $property = Property::paginate(6);
        return view('FrontHome' , compact('property'));
    }
    public function propertyDetail($id)
    {
        if(! is_numeric ($id)) return redirect()->back();
        $property = Property::select('properties.*', 'countries.country_name as country' , 'states.state_name as state' , 'cities.city_name as city')->where("id" , $id)->with(['specification','flat', 'imageArr'])
        ->join('countries' ,'properties.country','countries.country_id')
        ->join('states' , 'properties.state','states.state_id')
        ->join('cities','properties.city','cities.city_id')
        ->first();
        $amenity = explode(",",$property['amenity']);
        $amenityArr = [];
        foreach($amenity as $dt)
        {
            $adata = Amenity::select('icon' ,'name')->where('id',$dt)->first();
            if(!empty($adata)){
                $amenityArr[] =  $adata->toArray(); 
            }
        }
        $property['amenity'] = $amenityArr;
        return view('property-detail' , compact('property'));
    }
    public function getState($id)
    {
        $state = State::where("country_id",$id)->get();
        return response($state);
    }
    public function getcity($id)
    {
     
        $city = City::where("state_id",$id)->get();
        return response($city);
    }
    public function PropertyGrid(Property $property)
    {
        
        $property = Property::with('images')->orderBy( 'short_id')->paginate(15);
        
        return view('property-grid' , compact('property'));
    }
    public function propertyList()
    {
        $property = Property::with('images')->orderBy( 'short_id')->paginate(15);
        return view('property-list' , compact('property'));
    }
    public function ContactUs()
    {      
        return view('contact-us');
    }
    public function contactFrom(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required|numeric',
            'subject' => 'required',
            'message' => 'required',
        ]);

        $mail_data['mail_title'] ='Contact Us';
        $mail_data['mail_header'] = '';
        $mail_data['mail_msg'] = $request->toArray();
        $mail_data['title_msg'] = "Thanks for Contacting us";
        $data = array( 'body' => $mail_data);
        $to_name = $request->name;
        $to_email = "kishore43@gmail.com";
        $subject = $request->subject;
        Mail::send('contact-us-mail', $data, function($message) use ($to_name, $to_email, $subject , $request) {
            $message->to($to_email, $to_name)->cc( [$request->email , 'srisuprabhathambuilders@gmail.com'] )->subject('Contact us')->setBody( $request->message );
            $message->from($request->email);
        });
        $mail  = "Send Mail Successfully...";
        Session::flash('message', 'Thank you and we will contact you early as possible'); 
        return redirect('contact-us')->with($mail);

    }

    public function SubscribeUs(Request $request)
    {
        $request->validate([
            'semail' => 'required',
        ]);
            //srisuprabhathambuilders@gmail.com
        $data = array( 'body' => $request->message );
        
        $to_email = "kishore43@gmail.com ";
        $subject ='Subscribed - Successfully';
        Mail::send('sbscribe-us',[], function($message) use ($to_email, $subject , $request) {
            $message->to($to_email)->cc( [$request->semail , 'srisuprabhathambuilders@gmail.com'] )->subject($subject)->setBody('Thanks for Subscription with Sri Suprabhatham Builders. <br>
            We will keep you posted with the new projects in following up email.
            Thanks from SSB Team,  <br>
            Branch Office: No.21/1, 21st street,4th main road, nanganallur, Chennai - 600 061, <br>
            Head Office: No:19/9, Thambiah Reddy Road, Westmambalam, Chennai - 600033, <br>
            +919962482979,  044-22246564 / 42648288');
            $message->from($request->semail);
        });
        $mail = "Send Mail Successfully...";
        Session::flash('semail', 'Thank you for subscription'); 
        return redirect()->back()->with($mail);

    }

    public function jv(Request $request)
    {
        if (isPost()) {
            $request->validate([
                'owner_name' => 'required',
                'mediator' => 'required',
                'email' => 'required',
                'phone'  => 'required',
                'address' => 'required',
                'area' => 'required',
                'frontage' => 'required',
                'road_width' => 'required',
                'road_facing_direction' => 'required',
                'other_comment' => 'required'
            ]);
            $mail_data['mail_title'] =  'Joint Development';
            $mail_data['mail_header'] = '';
            $mail_data['mail_msg'] = $request->toArray();
            
            $data = array( 'body' => $mail_data);
            $to_name = $request->owner_name;
            $to_email = "kishore43@gmail.com";
            $subject = $request->other_comment;
            Mail::send( 'jv-mail', $data , function($message) use ($to_name, $to_email, $subject , $request) {
                    $message->to($to_email, $to_name)->cc( [$request->email,'srisuprabhathambuilders@gmail.com'])->subject('Joint Development')->setBody('');
                    $message->from( $request->email );
            });
            $mail  = "Send Mail Successfully...";
            Session::flash('message', 'Send Mail Successfully...'); 
            return redirect('jv')->with($mail);
        }
        else
        {
            return view('jv');
        }
    }
    public function sendMail(Request $request)
    {
        $request->validate(['email' => 'required',
                            'phone' => 'required|numeric',
                            'message' => 'required',
                             'name' => 'required']);

        $mail_data['mail_title'] ='Contact Us';
        $mail_data['mail_header'] = '';
        $mail_data['mail_msg'] = $request->toArray();
        $mail_data['mail_msg']['subject'] = "contact US";
        $mail_data['title_msg'] = "Enquiry for property ".$request['property_name'];
        $data = array( 'body' => $mail_data);
        $propertyName = $request['property_name'];
        $to_name = $request['name'];
        $to_email = "kishore43@gmail.com";
        $subject = "Property Enquiry $propertyName";
        Mail::send('contact-us-mail', $data, function($message) use ($to_name, $to_email, $subject , $request) {
            $message->to($to_email, $to_name)->cc( [$request->email , 'srisuprabhathambuilders@gmail.com'] )->subject($subject)->setBody( $request->message );
            $message->from($request->email);
        });
        $mail  = "Send Mail Successfully...";
        Session::flash('message', 'Thank you and we will contact you early as possible'); 
        return redirect('contact-us')->with($mail);
                    
            
       return response('Send Mail Successfully...');
        
                            

    }
    public function gallery()
    {
        return view('gallery');
    }
    public function aboutUs()
    { 
        return view('about-us');
    }
    public function bunglow()
    {
        
        $property = Property::where('label', 'LIKE', '%Bungalow%')->paginate(2);
        return view('bunglow' , compact('property'));
    }
}
