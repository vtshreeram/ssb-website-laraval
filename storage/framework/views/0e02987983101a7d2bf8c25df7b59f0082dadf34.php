<?php $__env->startSection('content'); ?>
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4 mb-3 border-bottom">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Amenity Edit</h3>
            </div>
        </div>
        <!-- End Page Header -->

        <div class="row">
            <div class="col-lg-12 mb-4">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Form</h6>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-3">
                            <div class="row">
                                <div class="col-sm-12 col-md-6">

                                    <form method="post" action="<?php echo e(url('ssb-admn/amenity/edit/' . $amenity['id'])); ?>"
                                        enctype="multipart/form-data">
                                        <?php echo csrf_field(); ?>
                                        <?php echo method_field('PUT'); ?>
                                        <div class="form-group">
                                            Name
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" name="name" aria-label="Username"
                                                    aria-describedby="basic-addon1" value="<?php echo e($amenity['name']); ?>"> </div>
                                        </div>
                                        <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <em class="text-danger"><?php echo e($message); ?></em>
                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                        <div class="image">
                                            <img src="<?php echo e(asset('public/images/icon/' . $amenity['icon'])); ?>" width="100"
                                                height="100" id="artPrevImage_00" class="image"
                                                style="margin-bottom:0px;padding:3px;" alt="logo" /><br />
                                            <input type="file" name="icon" id="ariImg_00" onchange="readURL(this,'00');"
                                                style="display: none;" accept="image/jpg,image/png">
                                            <input type="hidden"
                                                value="<?php echo @$cd_logo ? $cd_logo : @$_POST['cd_logo']; ?>"
                                                name="cd_logo" id="hiddenArtImgLogo" />
                                            <a
                                                onclick="$('#ariImg_00').trigger('click');">Browse</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a
                                                style="clear:both;"
                                                onclick="javascript:clear_image('artPrevImage_00')">Clear</a>
                                        </div>
                                        <?php $__errorArgs = ['icon'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <em class="text-danger"><?php echo e($message); ?></em>
                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                        <div class="col mb-4">
                                            <button type="submit" class="bg-success rounded text-white text-center p-3"
                                                style="box-shadow: inset 0 0 5px rgba(0,0,0,.2);">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/srisupra/web/srisuprabhathambuilder.com/public_html/resources/views/amenity/edit.blade.php ENDPATH**/ ?>