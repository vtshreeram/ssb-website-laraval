<!doctype html>
<html class="no-js h-100" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>SSB-2</title>
    <meta name="description" content="A high-quality &amp; free Bootstrap admin dashboard template pack that comes with lots of templates and components.">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" rel="stylesheet">
    <link rel='stylesheet' href='https://harvesthq.github.io/chosen/chosen.css'>
      <link rel="stylesheet" href="<?php echo e(asset('public/css/style.css')); ?>">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-3.3.1/jszip-2.5.0/dt-1.10.22/af-2.3.5/b-1.6.4/b-flash-1.6.4/b-html5-1.6.4/b-print-1.6.4/r-2.2.6/sl-1.3.1/datatables.min.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" id="main-stylesheet" data-version="1.1.0" href="<?php echo e(asset('public/css/shards-dashboards.1.1.0.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('public/css/extras.1.1.0.min.css')); ?>">
    <script async defer src="https://buttons.github.io/buttons.js"></script>
      <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <?php echo $__env->yieldContent('css'); ?>
    <script>
      var url = '<?php echo e(url('/')); ?>';
      var public = '<?php echo e(url('public')); ?>';
    </script>
    <style>
      .icon
      {
        width: 30px;
        height: auto;
      }
      .loder{
        height: 100vh;
        width: 100vh;
        overflow: hidden;
        display: none;
        position: fixed;
      }
      .loder{
        top: 0;
        left: 0;
        background-image: url('<?php echo e(asset("public/images/loder.gif")); ?>');
        background-position: center;
        width: 100%;
        z-index: 99999;
        background-color: rgba(0,0,0,0.3);
        background-repeat: no-repeat;
      }
      .is-Loder .loder{
        display: block;
      }
      .old-images , .old-pdf{
            display: flex;
            flex-wrap: wrap;
        }
        .old-images div.img-layout ,.old-pdf div.pdf-layout{
            margin: 5px;
            position: relative;
        }
        .old-images div.img-layout .img-delete ,.old-pdf div.pdf-layout .pdf-delete{
            position: absolute;
            right: 5px;
            top: -10px;
        }
        .old-images div.img-layout img ,.old-pdf div.pdf-layout img{
            height: 100px;
            width: 100px;
        }
    </style>
  </head>
  <body class="h-100">
    <div class="loder"></div>
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
          <div class="main-navbar">
            <nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
              <a class="navbar-brand w-100 mr-0" href="#" style="line-height: 25px;">
                <div class="d-table m-auto">
                  <img id="main-logo" class="d-inline-block align-top mr-1" style="max-width: 25px;" src="<?php echo e(asset('public/front/images/sri-logo.png')); ?>" alt="Shards Dashboard">
                  <span class="d-none d-md-inline ml-1">SSB</span>
                </div>
              </a>
              <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
                <i class="material-icons">&#xE5C4;</i>
              </a>
            </nav>
          </div>
          <form action="#" class="main-sidebar__search w-100 border-right d-sm-flex d-md-none d-lg-none">
            <div class="input-group input-group-seamless ml-3">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <i class="fas fa-search"></i>
                </div>
              </div>
              <input class="navbar-search form-control" type="text" placeholder="Search for something..." aria-label="Search"> </div>
          </form>
          <div class="nav-wrapper">
            <ul class="nav flex-column">
              <li class="nav-item">
              <a class="nav-link <?php echo e((str_contains(Request::fullUrl(), 'specification')) ? 'active' : ''); ?>" href="<?php echo e(url('ssb-admn/specification')); ?>">
                  <i class="material-icons">edit</i>
                  <span>Specification</span>
                </a>
              </li>
              <li class="nav-item">
              <a class="nav-link <?php echo e((str_contains(Request::fullUrl(), 'amenity')) ? 'active' : ''); ?>" href="<?php echo e(url('ssb-admn/amenity')); ?>">
                  <i class="material-icons">vertical_split</i>
                  <span>Amenity</span>
                </a>
              </li>
              <li class="nav-item d-none">
                <a class="nav-link " href="<?php echo e(url('flat')); ?>">
                  <i class="material-icons">note_add</i>
                  <span>Flats</span>
                </a>
              </li>
              <li class="nav-item">
              <a class="nav-link <?php echo e((str_contains(Request::fullUrl(), 'property')) ? 'active' : ''); ?>" href="<?php echo e(url('ssb-admn/property')); ?>">
                  <i class="material-icons">view_module</i>
                  <span>Property</span>
                </a>
              </li>
            </ul>
          </div>
        </aside>
        <!-- End Main Sidebar -->
        <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
          <div class="main-navbar sticky-top bg-white">
            <!-- Main Navbar -->
            <nav class="navbar align-items-stretch navbar-light flex-md-nowrap p-0">
              <form action="#" class="main-navbar__search w-100 d-none d-md-flex d-lg-flex">
                <div class="input-group input-group-seamless ml-3">
                </div>
              </form>
              <ul class="navbar-nav border-left flex-row ">
                <li class="nav-item dropdown d-flex align-items-center">
                  <a class="nav-link dropdown-toggle text-nowrap px-3" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">

                      <span class="d-none d-md-inline-block"><?php echo e(__('Profile')); ?></span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-small">
                  <a class="dropdown-item" href="<?php echo e(url('profile')); ?>">
                      <i class="material-icons">&#xE7FD;</i> Profile</a>
                    <a class="dropdown-item" href="components-blog-posts.html">
                      <i class="material-icons">vertical_split</i> Blog Posts</a>
                    <a class="dropdown-item d-none" href="add-new-post.html">
                      <i class="material-icons">note_add</i> Add New Post</a>
                    <div class="dropdown-divider"></div>
                    <form method="POST" action="<?php echo e(route('logout')); ?>">
                      <?php echo csrf_field(); ?>
                    <a class="dropdown-item text-danger" href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();this.closest('form').submit();">
                      <i class="material-icons text-danger">&#xE879;</i> Logout </a>
                    </form>
                  </div>
                </li>
              </ul>
              <nav class="nav">
                <a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
                  <i class="material-icons">&#xE5D2;</i>
                </a>
              </nav>
            </nav>
          </div>
          <?php echo $__env->yieldContent('content'); ?>
        </main>
      </div>
    </div>


  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
    <script src="https://unpkg.com/shards-ui@latest/dist/js/shards.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Sharrre/2.0.1/jquery.sharrre.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/js/all.min.js" integrity="sha512-YSdqvJoZr83hj76AIVdOcvLWYMWzy6sJyIMic2aQz5kh2bPTd9dzY3NtdeEAzPp/PhgZqr4aJObB3ym/vsItMg==" crossorigin="anonymous"></script>
    <script src="<?php echo e(asset('public/js/extras.1.1.0.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/js/shards-dashboards.1.1.0.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/js/admin.js')); ?>"></script>
    <script src="<?php echo e(asset('public/js/app/app-blog-overview.1.1.0.js')); ?>"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-3.3.1/jszip-2.5.0/dt-1.10.22/af-2.3.5/b-1.6.4/b-flash-1.6.4/b-html5-1.6.4/b-print-1.6.4/r-2.2.6/sl-1.3.1/datatables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            $('.datatable').DataTable({
                "lengthChange": false,
                "info": false,
                "ordering": false,
                "searching": true,
                "language": {
                    "paginate": {
                        "next": "Next",
                        "previous": "prev"
                    }
                }
            });
          setTimeout(function(){ $(".notification").hide(); }, 5000);
          $("#country").change(function (e) { 
                    var id = $(this).children("option:selected").val();
                    console.log($(this).val());
                    $.ajax({
                        type: "get",
                        url: base_url+'/getState/'+id,
                        success: function (response) {
                          $("#state").empty();
                            $.each(response, function (i, v) { 
                                $("#state").append(`<option value='${v.state_id}'>${v.state_name}</option>`);
                            });
                          
                        }
                    });
                    
                });
                $("#state").change(function (e) { 
                    var id = $(this).children("option:selected").val();
                    $.ajax({
                        type: "get",
                        url: base_url+'/getcity/'+id,
                        success: function (response) {
                          $("#city").empty();
                            $.each(response, function (i, v) { 
                                $("#city").append(`<option value='${v.city_id}'>${v.city_name}</option>`);
                            });
                          
                        }
                    });
                    
                });
                
        });
       
    </script>
    <?php echo $__env->yieldContent('script'); ?>
  </body>
</html>
<?php /**PATH /home/srisupra/web/srisuprabhathambuilder.com/public_html/resources/views/layouts/admin.blade.php ENDPATH**/ ?>