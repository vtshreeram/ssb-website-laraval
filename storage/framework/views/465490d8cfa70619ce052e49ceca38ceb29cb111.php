


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SSB-Login</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo e(asset('public/css/login.css')); ?>">
</head>
<body>

<div class="log">
    <?php if(session('status')): ?>
    <div class="mb-4 font-medium text-sm text-green-600">
        <?php echo e(session('status')); ?>

    </div>
    <?php endif; ?>
    <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
        <em class="isInvalid"><?php echo e($message); ?></em>
    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
    <h2>Login</h2>
    <form method="POST" action="<?php echo e(route('login')); ?>">
        <?php echo csrf_field(); ?>
      <div class="input-cont">
        <input type="text" name="email" placeholder="E-mail" value="<?php echo e(old('email')); ?>" required>
        <div class="border1"></div>
      </div>
      <div class="input-cont">
        <input type="password" name="password" placeholder="Password" required autocomplete="current-password">
        <div class="border2"></div>
      </div>
      
      <div class="clear"></div>
      <input type="submit" value="Login">
    </form>
</div>
  
</body>
</html><?php /**PATH /home/srisupra/web/srisuprabhathambuilder.com/public_html/resources/views/auth/login.blade.php ENDPATH**/ ?>