<?php $__env->startSection('content'); ?>
    <!-- / .main-navbar -->
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-6 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">Amenity Llist</h3>
            </div>
            <div class="col-6 col-sm-8 text-center text-sm-left mb-0 d-flex justify-content-end align-items-center">
                <a href="<?php echo e(url('ssb-admn/amenity/create')); ?>" class="bg-success rounded text-white text-center py-2 px-3 d-inline-block"
                    style="box-shadow: inset 0 0 5px rgba(0,0,0,.2);">Add</a>
            </div>
        </div>
        <!-- End Page Header -->
        <!-- Default Light Table -->
        
        <?php if(Session::has('message')): ?> <div class="alert <?php echo e(Session::get('class')); ?> notification"><strong><?php echo e(Session::get('message')); ?></strong></div><?php endif; ?>
        <div class="row">
            <div class="col">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Active Users</h6>
                    </div>
                    <div class="card-body p-0 pb-3">
                        <table class="table mb-0 datatable">
                            <thead class="bg-light">
                                <tr>
                                    <th scope="col" class="border-0">id</th>
                                    <th scope="col" class="border-0">name</th>
                                    <th scope="col" class="border-0">icon</th>
                                    <th scope="col" class="border-0">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($amenityArr)): ?>
                                    <?php $__currentLoopData = $amenityArr; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($k+1); ?></td>
                                            <td><?php echo e($item->name); ?></td>
                                            <td><img class="icon" src="<?php echo e(asset('public/images/icon/'.$item->icon)); ?>"> </td>
                                           <td><a href="<?php echo e(url('ssb-admn/amenity/edit/'.$item->id)); ?>"><i class="far fa-edit"></i></i></a> <a href="<?php echo e(url('ssb-admn/amenity/delete/'.$item->id)); ?>"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Default Light Table -->

    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/srisupra/web/srisuprabhathambuilder.com/public_html/resources/views/amenity/index.blade.php ENDPATH**/ ?>