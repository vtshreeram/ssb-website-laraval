<?php $__env->startSection('content'); ?>
    
<!--=================================
breadcrumb -->
<div class="bg-light">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="index.html"> <i class="fas fa-home"></i> </a></li>
            
            <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span> Jv </span></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!--=================================
  breadcrumb -->
  
  <!-- Jv Form Start -->
  <section class="my-5">
     
    <form action="<?php echo e(url('jv')); ?>" method="post">
        <?php echo csrf_field(); ?> 
  <div class="container">
  
      <div class="joint-description text-center">
      <h1>joint development</h1>
      <p>If you are interested in promoting your property by way of Joint Development or outright sale, contact us for the best offer in the market.
      ( All information given below will be treated in strict confidence with the management and the owner of the property, kindly go ahead and fill the below form and submit ) </p>
      <p>(Fields Marked with * are mandatory) </p>
      </div>
      <?php if(Session::has('message')): ?> <div class="alert alert-success"><strong><?php echo e(Session::get('message')); ?></strong></div><?php endif; ?>
      <div class="form-box">
          <div class="form-header">
               <h5>Basic Details</h5> 
              <div class="line"></div>
          </div>
  
        
              <div class="row mt-3">
                  <div class="col-md-4 mt-3">
                      <div class="owner">
                      <input type="text" class="form-control" value="<?php echo e(old('owner_name')); ?>" name="owner_name" placeholder="Name of owner*">
                      </div>
                      <?php $__errorArgs = ['owner_name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                      <em class="text-danger"><?php echo e($message); ?></em>  
                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                  </div>
                 
                  <div class="col-md-4 mt-3">
                      <div class="owner">
                          <input type="text" class="form-control"  value="<?php echo e(old('mediator')); ?>"  name="mediator" placeholder="Name of mediator*">
                      </div>
                      <?php $__errorArgs = ['mediator'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <em class="text-danger"><?php echo e($message); ?></em>  
                  <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                  </div>
                  
                  <div class="col-md-4 mt-3">
                      <div class="owner">
                          <input type="text" class="form-control" name="email"  value="<?php echo e(old('email')); ?>" placeholder="Email*">
                      </div>
                      <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <em class="text-danger"><?php echo e($message); ?></em>  
                  <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                  </div>
                  
                  <div class="col-md-4 mt-3">
                      <div class="owner">
                          <input type="text" class="form-control" name="phone"  value="<?php echo e(old('phone')); ?>" placeholder="Phone*">
                      </div>
                      <?php $__errorArgs = ['phone'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <em class="text-danger"><?php echo e($message); ?></em>  
                  <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                  </div>
                  
                  <div class="col-md-4 mt-3">
                      <div class="owner">
                          <input type="text" class="form-control" name="address" value="<?php echo e(old('address')); ?>" placeholder="Address*">
                      </div>
                      <?php $__errorArgs = ['address'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <em class="text-danger"><?php echo e($message); ?></em>  
                  <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                  </div>
                  
                  <div class="col-md-4 mt-3">
                      <div class="owner">
                          <input type="text" class="form-control" name="area" value="<?php echo e(old('area')); ?>" placeholder="Area*">
                      </div>
                      <?php $__errorArgs = ['area'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <em class="text-danger"><?php echo e($message); ?></em>  
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                  </div>
                 
              </div>
  
              <div class="form-header padding-top my-4">
                  <h5>Dimension</h5>
              </div>
  
              <div class="form-row">
                  <div class="col-md-3 mt-3">
                      <div class="owner">
                          <input type="text" class="form-control" name="north" value="<?php echo e(old('north')); ?>"  placeholder="North"> 
                      </div>
                      
                  </div>
                 
                  <div class="col-md-3 mt-3">
                      <div class="owner">
                          <input type="text" class="form-control" name="south" value="<?php echo e(old('south')); ?>" placeholder="South"> 
                      </div>
                  </div>
                  
                  <div class="col-md-3 mt-3">
                      <div class="owner">
                          <input type="text" class="form-control" name="east"  value="<?php echo e(old('east')); ?>" placeholder="East"> 
                      </div>
                  </div>
  
                  <div class="col-md-3 mt-3">
                      <div class="owner">
                          <input type="text" class="form-control" name="west"  value="<?php echo e(old('west')); ?>"  placeholder="West"> 
                      </div>
                  </div>
  
                  <div class="col-md-3 mt-3">
                      <div class="owner">
                          <input type="text" class="form-control" name="frontage" value="<?php echo e(old('frontage')); ?>"  placeholder="Frontage*"> 
                      </div>
                      <?php $__errorArgs = ['frontage'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <em class="text-danger"><?php echo e($message); ?></em>  
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                  </div>
                  
                  <div class="col-md-3 mt-3">
                      <div class="owner">
                          <input type="text" class="form-control" name="road_width"  value="<?php echo e(old('road_width')); ?>" placeholder="Road Width*"> 
                      </div>
                      <?php $__errorArgs = ['road_width'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <em class="text-danger"><?php echo e($message); ?></em>  
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                  </div>
                 
                  <div class="col-md-3 mt-3">
                      <div class="owner">
                          <input type="text" class="form-control" name="road_facing_direction"   value="<?php echo e(old('road_facing_direction')); ?>" placeholder="Road Facing Direction*"> 
                      </div>
                      <?php $__errorArgs = ['road_facing_direction'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <em class="text-danger"><?php echo e($message); ?></em>  
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                  </div>
                
              </div>
          </form>
      </div>
  
      <div class="form-box">
  
      <div class="form-header">
              <h1>Terms Interested</h1>
              <div class="line"></div>
      </div>
  
      <div class="row">
          <div class="col-md-4">
              <!-- <div class="form-group property-price-slider mt-3">
                    <label>Select Price Range</label>
                    <input type="text" id="property-price-slider" name="example_name" value="" />
              </div> -->
          </div>
  
          <div class="col-md-4">
              <!-- <div class="form-group property-price-slider123 mt-3">
                    <label>Select Price Range</label>
                    <input type="text" id="property-price-slider123" />
              </div> -->
          </div>
      </div>
      
  
      <div class="form-row margin-row-top">
      <div class="col-md-4">
      <div class="ratio">
      <h6>Joint Development</h6>
      <!-- <input id="yes" name="yes" type="checkbox" value="yes" />
      <label for="yes">Yes</label> -->
      <!-- <div class="form-group12">
        <input type="checkbox" id="html">
        <label for="html">Yes</label>
      </div> -->
      <div class="custom-control custom-checkbox ml-2">
              <input type="checkbox" class="custom-control-input" name="joint_development" id="customCheck1">
              <label class="custom-control-label pr-5" for="customCheck1">Yes</label>
      </div>
      </div>
      <div class="expected-ratio">
      <h5>Expected Ratio</h5>
  
      <div class="form-group property-price-slider mt-3">
              <label>Select Price Range</label>
              <input type="text" id="property-price-slider" name="price_range"  min="0" max="500" />
             
      </div>
  
  
      <!-- <input class="range" max="100" min="0" step="1" type="range" value="10" />
      <input id="rangevalue1" class="range-input" maxlength="3" type="text" /> -->
  
  
      </div>
      </div>
  
      <div class="col-md-4">
      <div class="ratio">
      <h6>Out Right Purchased</h6>
      <!-- <input id="yes-right" name="yes-right" type="checkbox" value="yes-right" /> <label for="yes-right">Yes</label> -->
      <!-- <div class="form-group12">
        <input type="checkbox" id="css">
        <label for="css">Yes</label>
      </div> -->
      <div class="custom-control custom-checkbox ml-2">
                    <input type="checkbox" class="custom-control-input" name="out_right_purchase" id="customCheck2">
                    <label class="custom-control-label pr-5" for="customCheck2">Yes
                    </label>
      </div>
      </div>
  
      <div class="expected-ratio">
      <h5>Rate per Ground</h5>
      <!-- <input class="range" max="100" min="0" step="1" type="range" value="10" />
      <input id="rangevalue1" class="range-input" maxlength="12" type="text" />
       -->
  <div class="form-group property-price-slider123 mt-3">
        <label>Select Price Range</label>
        <input type="text" id="property-price-slider123" name="property_price_slider" />
  </div>
  
      
      <strong>Price Range:</strong>₹75,00,000 to ₹15,000,000
  
      <!-- <input type="text" id="property-price-slider" name="example_name" value="" /> -->
      
      </div>
      </div>
  
      <div class="col-md-4">
          <div class="owner">
          <label>Other Comment*</label>
          <textarea class="form-control" rows="3" name="other_comment"  spellcheck="false"><?php echo e(old('other_comment')); ?></textarea>
          </div>
          <?php $__errorArgs = ['other_comment'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
            <em class="text-danger"><?php echo e($message); ?></em>
          <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
          <div id="recaptcha" class="captcha-contact col-md-12 mt-5 g-recaptcha"></div> 
            <em class="text-danger" id="recaptcha_msg"></em>    
      </div>
      
      </div>
      
      <div align="right">
          <button id="joint_developemnt_form" class="send-form-data" type="submit">Send</button>
      </div>
    </form>
  
  
  
      
      
  </div>
  </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
    </script>
<script>
  
var flag =  false;
$("#joint_developemnt_form").click(function (e) { 

if(flag)
{
$("#recaptcha_msg").text("");
}
else
{
$("#recaptcha_msg").text("Please checked Recaptcha");
  return false;
}
});

// var verifyCallback = function(response) {
//  flag  = true;
//  $("#recaptcha_msg").text("");
// };
var widgetId1;
var widgetId2;
// var onloadCallback = function() {
// // Renders the HTML element with id 'example1' as a reCAPTCHA widget.
// // The id of the reCAPTCHA widget is assigned to 'widgetId1'.
// grecaptcha.render('recaptcha', {
//  'sitekey' : '6Ldu6c8ZAAAAAIbXoD48KNPQFI6jVkZjzAMuUPp3',
//  'callback' : verifyCallback,
//  'theme' : 'light'
// });
// };
</script>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      xfbml            : true,
      version          : 'v9.0'
    });
  };

  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your Chat Plugin code -->
<div class="fb-customerchat"
  attribution=setup_tool
  page_id="105584477968961"
theme_color="#f64546"
logged_in_greeting="You can dream, create, design, and build the most wonderful place in the world."
logged_out_greeting="You can dream, create, design, and build the most wonderful place in the world.">
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/srisupra/web/srisuprabhathambuilder.com/public_html/resources/views/jv.blade.php ENDPATH**/ ?>