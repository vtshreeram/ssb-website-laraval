<?php $__env->startSection('css'); ?>
<script type="text/javascript" src="<?php echo e(asset('public/js/ckeditor/ckeditor.js')); ?>"></script>
<script type="text/javascript">
	$(document).ready(function(e) {
	    var description = document.getElementById('description');
	   	CKEDITOR.replace( 'description',
	    {
	        filebrowserBrowseUrl : 'kcfinder/browse.php',
	        filebrowserImageBrowseUrl : 'kcfinder/browse.php?type=Images',
	        filebrowserUploadUrl : 'kcfinder/upload.php',
	        filebrowserImageUploadUrl : 'kcfinder/upload.php?type=Images'
			
		
	    });
    });
</script>    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4 mb-3 border-bottom">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Specification</h3>
            </div>
        </div>
        <!-- End Page Header -->

        <div class="row">
            <div class="col-lg-12 mb-4">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Form</h6>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-3">
                            <div class="row">
                                <div class="col-sm-12 ">
                                    <form method="post" action="<?php echo e(url('ssb-admn/specification/store')); ?>" enctype="multipart/form-data">
                                        <?php echo csrf_field(); ?>
                                        <div class="form-group mb-3">
                                             <label>Name</label>
                                            <div class="input-group">
                                               
                                                <input type="text" class="form-control" name="name"
                                                    aria-label="Username" aria-describedby="basic-addon1"> </div>
                                                    <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                        <em class="text-danger"><?php echo e($message); ?></em>
                                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea type="text" name="description" id="description" class="form-control" placeholder="Description" value="myCoolPassword"> </textarea>
                                        </div>
                                        <label>Image</label>
                                        <div class="image">
                                            <img src="<?php echo e(asset('public/images/no-image.png')); ?>" width="100" height="100" id="artPrevImage_00" class="image" style="margin-bottom:0px;padding:3px;" alt="logo" /><br />
        									<input type="file" name="icon" id="ariImg_00" onchange="readURL(this,'00');" style="display: none;" accept="image/jpg,image/png">
                                            <a onclick="$('#ariImg_00').trigger('click');">Browse</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a style="clear:both;" onclick="javascript:clear_image('artPrevImage_00')">Clear</a>
                                        </div>
                                        <?php $__errorArgs = ['icon'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                            <em class="text-danger"><?php echo e($message); ?></em>
                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                        <div class="col mb-4 d-flex justify-content-end">
                                            <button type="submit" class="bg-success rounded text-white text-center py-2 px-3 d-inline-block border-0"
                                                style="box-shadow: inset 0 0 5px rgba(0,0,0,.2);">Save</button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/srisupra/web/srisuprabhathambuilder.com/public_html/resources/views/specification/create.blade.php ENDPATH**/ ?>