<?php $__env->startSection('content'); ?>
<?php $__env->startSection('css'); ?>
    <style>
        select {
            width: 100%;
        }
    </style>
    <script type="text/javascript" src="<?php echo e(asset('public/js/ckeditor/ckeditor.js')); ?>"></script>
<script type="text/javascript">
	$(document).ready(function(e) {
	    var description = document.getElementById('sp_description');
	   	CKEDITOR.replace( description,
	    {
	        filebrowserBrowseUrl : 'kcfinder/browse.php',
	        filebrowserImageBrowseUrl : 'kcfinder/browse.php?type=Images',
	        filebrowserUploadUrl : 'kcfinder/upload.php',
	        filebrowserImageUploadUrl : 'kcfinder/upload.php?type=Images'
			
		
	    });
	     var description = document.getElementById('edit_description');
	   	CKEDITOR.replace( description,
	    {
	        filebrowserBrowseUrl : 'kcfinder/browse.php',
	        filebrowserImageBrowseUrl : 'kcfinder/browse.php?type=Images',
	        filebrowserUploadUrl : 'kcfinder/upload.php',
	        filebrowserImageUploadUrl : 'kcfinder/upload.php?type=Images'
			
		
	    });
    });
</script>
<?php $__env->stopSection(); ?>
<?php
$specificationRow = 0;
$flat = 0;
?>
<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4 mb-3 border-bottom">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <h3 class="page-title">Property</h3>
        </div>
    </div>
    <!-- End Page Header -->
<!-- Modal -->
<div class="modal fade theam-modal" id="edit_specification" tabindex="-1" role="dialog" aria-labelledby="add-ShippingBillLabel" aria-hidden="true">
	<div class="modal-dialog model-martop1" role="document">
		<div class="modal-content model-martop1">
			<div class="modal-header">
				<h4 class="modal-title text-uppercase" id="add-ShippingBillLabel">Specification Data</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<input type="hidden" name="row_id" id="hidden_row_id" value="" />
				<div class="form-group">
					<label>Specification</label>
                    <select class="form-control" id="edit_sp_name" disabled >
                        <option value="">Choose...</option>
                        <?php $__currentLoopData = $Specification; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($item['id']); ?>"><?php echo e($item['name']); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
				</div>
				<div class="form-group">
					<label>Description</label>
					<textarea class="form-control" id="edit_description" style="resize: none;"></textarea>
				</div>
			</div>
			<div class="modal-footer border-top-0">
				<button type="button" id="edit" name="edit" class="btn btn-success btn-width" value="Edit"> Save</button>
				<button type="button" class="btn btn-danger btn-width" data-dismiss="modal"> Cancel
				</button>
			</div>
		</div>
	</div>
</div>
<!-- end Edit Modal -->
<!-- Modal -->
<div class="modal fade theam-modal" id="edit_flat" tabindex="-1" role="dialog" aria-labelledby="add-ShippingBillLabel" aria-hidden="true">
	<div class="modal-dialog model-martop1" role="document">
		<div class="modal-content model-martop1">
			<div class="modal-header">
				<h4 class="modal-title text-uppercase" id="add-ShippingBillLabel">Flat Data</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
                <input type="hidden" name="row_id" id="hidden_row_id" value=""/>
                <div class="form-group">
                    <label>floor</label>
                    <select class="form-control" id="edit_floor">
                        <option value="">Choose...</option>
                        <option value="GF">GF</option>
                        <option value="I">I</option>
                        <option value="II">II</option>
                        <option value="III">III</option>
                        <option value="IV">IV</option>
                        <option value="V">V</option>
                        <option value="VI">VI</option>
                        <option value="VII">VII</option>
                        <option value="VIII">VIII</option>
                        <option value="IX">IX</option>
                        <option value="X">X</option>
                     </select>
				</div>
                <div class="form-group">
					<label>Square Fit</label>
					<input class="form-control" id="edit_sqft" style="resize: none;"/>
                </div>
                <div class="form-group">
					<label>BHk</label>
					<input class="form-control" id="edit_bhk" style="resize: none;" onkeypress="return isNumber(event)"/>
                </div>
                <div class="form-group">
					<label>Status</label>
                    <select class="form-control" id="edit_status">
                        <option value="">Choose...</option>
                        <option value="1">Sold</option>
                        <option value="2">Avalilable</option>
                    </select>
				</div>
			</div>
			<div class="modal-footer border-top-0">
				<button type="button" id="edit_flat_save" class="btn btn-success btn-width" value="Edit"> Save</button>
				<button type="button" class="btn btn-danger btn-width" data-dismiss="modal"> Cancel
				</button>
			</div>
		</div>
	</div>
</div>
<!-- end Edit Modal -->
    <div class="row">
        <div class="col-lg-12 mb-4">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom my-3">
                    <h4 class="m-0 text-uppercase">Add Property</h4>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                        <div class="row">
                            <div class="col-sm-12">
                                <form method="post" action="<?php echo e(url('ssb-admn/property/store')); ?>" enctype="multipart/form-data" autocomplete="on">
                                    <?php echo csrf_field(); ?>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Name <em class="text-danger">*</em></label>
                                            <div class="form-group mb-3">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="property_name" value="<?php echo e(old('property_name')); ?>" aria-label="Username" aria-describedby="basic-addon1" required="required">
                                                </div>
                                                <?php $__errorArgs = ['property_name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                	<em class="text-danger"><?php echo e($message); ?></em>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <label>Address <em class="text-danger">*</em></label>
                                            <div class="form-group mb-3">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="address" value="<?php echo e(old('address')); ?>" aria-label="Username" aria-describedby="basic-addon1" required="required">
                                                </div>
                                                <?php $__errorArgs = ['address'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                	<em class="text-danger"><?php echo e($message); ?></em>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                        </div>

                                    </div>
                                   
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Square Fit <em class="text-danger">*</em></label>
                                            <div class="form-group mb-3">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="property_sqft" value="<?php echo e(old('property_sqft')); ?>" aria-label="Username" aria-describedby="basic-addon1" required="required">
                                                </div>
                                                <?php $__errorArgs = ['property_sqft'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                <em class="text-danger"><?php echo e($message); ?></em>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Amount <em class="text-danger">*</em></label>
                                            <div class="form-group mb-3">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="amount" value="<?php echo e(old('amount')); ?>" onkeypress="return isNumber(event)" aria-label="Username" aria-describedby="basic-addon1">
                                                </div>
                                                <?php $__errorArgs = ['amount'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                	<em class="text-danger"><?php echo e($message); ?></em>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Bed <em class="text-danger">*</em></label>
                                            <div class="form-group mb-3">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="bed" value="<?php echo e(old('bed')); ?>" onkeypress="return isNumber(event)" aria-label="Username" aria-describedby="basic-addon1" required="required">
                                                </div>
                                                <?php $__errorArgs = ['bed'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                	<em class="text-danger"><?php echo e($message); ?></em>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Bath <em class="text-danger">*</em></label>
                                            <div class="form-group mb-3">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="bath" value="<?php echo e(old('bath')); ?>" onkeypress="return isNumber(event)" aria-label="Username" aria-describedby="basic-addon1" required="required">
                                                </div>
                                                <?php $__errorArgs = ['bath'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                <em class="text-danger"><?php echo e($message); ?></em>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Country <em class="text-danger">*</em></label>
                                            <div class="form-group">
                                                <select class="form-control" id="country" name="country">
                                                    <option>Choose...</option>
                                                    <?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($item->country_id); ?>"><?php echo e($item->country_name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                                <?php $__errorArgs = ['country'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                	<em class="text-danger"><?php echo e($message); ?></em>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>State <em class="text-danger">*</em></label>
                                            <div class="form-group">
                                                <select class="form-control" id="state" name="state">
                                                    <option value="">Choose...</option>
                                                </select>
                                                <?php $__errorArgs = ['state'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                	<em class="text-danger"><?php echo e($message); ?></em>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>City <em class="text-danger">*</em></label>
                                            <div class="form-group">
                                                <select class="form-control" id="city" name="city">
                                                    <option value="">Choose...</option>
                                                </select>
                                                <?php $__errorArgs = ['city'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                	<em class="text-danger"><?php echo e($message); ?></em>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Area <em class="text-danger">*</em></label>
                                            <div class="form-group mb-3">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="area" value="<?php echo e(old('area')); ?>" aria-label="Username" aria-describedby="basic-addon1">
                                                </div>
                                                <?php $__errorArgs = ['area'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                	<em class="text-danger"><?php echo e($message); ?></em>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Pincode <em class="text-danger">*</em></label>
                                            <div class="form-group mb-3">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="pincode" value="<?php echo e(old('pincode')); ?>" aria-label="Username" aria-describedby="basic-addon1" required="required">
                                                </div>
                                                <?php $__errorArgs = ['pincode'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                	<em class="text-danger"><?php echo e($message); ?></em>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <label>Label <em class="text-danger">*</em></label>
                                                  <div id="output"></div>
                                                  <select data-placeholder="Choose Label ..." name="label[]" multiple class="chosen-select">
                                                      <option value="Bungalow" <?php echo e((!empty(old('label'))) ? (in_array("Bungalow",old('label'))) ? "selected" : "" : ""); ?>>Bungalow</option>
                                                      <option value="Completed" <?php echo e((!empty(old('label'))) ? (in_array("Completed",old('label'))) ? "selected" : "" : ""); ?>>Completed</option>
                                                      <option value="Sold Out" <?php echo e((!empty(old('label'))) ? (in_array("Sold Out",old('label'))) ? "selected" : "" : ""); ?>>Sold Out</option>
                                                      <option value="On going" <?php echo e((!empty(old('label'))) ? (in_array("On going",old('label'))) ? "selected" : "" : ""); ?>>On going</option>
                                                  </select>
                                                  <?php $__errorArgs = ['label'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                         <em class="text-danger"><?php echo e($message); ?></em>
                                                  <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                          </div>
                                        
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Property Images <em class="text-danger">*</em></label>
                                            <div id="imageData">
                                                <div class="image">
                                                    <img src="<?php echo e(asset('public/images/no-image.png')); ?>" width="100" height="100" id="artPrevImage_00" class="image" style="margin-bottom:0px;padding:3px;" alt="logo" /><br />
                                                    <input type="file" name="image[]" id="ariImg_00" onchange="readURL(this,'00');" style="display: none;" accept="image/jpg,image/png">
                                                    <input type="hidden" value="" name="image" id="hiddenArtImgLogo" required="required"/>
                                                    <a onclick="$('#ariImg_00').trigger('click');">Browse</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                    <a style="clear:both;" onclick="javascript:clear_image('artPrevImage_00')">Clear</a>
                                                </div>
                                            </div>
                                            <button id="add_image" onclick="return false;" class="btn btn-primary my-3">Add Image<i class="fa fa-plus-circle ml-2" aria-hidden="true"></i></button>
                                            <?php $__errorArgs = ['image'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                            	<em class="text-danger"><?php echo e($message); ?></em>
                                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Floor Plan <em class="text-danger">*</em></label>
                                            <div class="form-group">
                                                <div class="image">
                                                    <img src="<?php echo e(asset('public/images/pdf.png')); ?>" width="100" height="100" id="artPrevImage_111" class="image" style="margin-bottom:0px;padding:3px;" alt="logo" /><br />
                                                    <input type="file" name="floor_plan" id="ariImg_111" onchange="readURL(this,'111',1);" style="display: none;" accept=".pdf">
                                                    <input type="hidden" value="" name="cd_logo" id="hiddenArtImgLogo" required="required"/>
                                                    <a onclick="$('#ariImg_111').trigger('click');">Browse</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                    <a style="clear:both;" onclick="javascript:clear_image('artPrevImage_111' ,'pdf.png')">Clear</a>
                                                </div>
                                                <?php $__errorArgs = ['floor_plan'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                	<em class="text-danger"><?php echo e($message); ?></em>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Video</label>
                                            <div class="form-group mb-3">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="video" value="<?php echo e(old('video')); ?>">
                                                </div>
                                                <?php $__errorArgs = ['video'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                	<em class="text-danger"><?php echo e($message); ?></em>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Map</label>
                                            <div class="form-group mb-3">
                                                <div class="input-group">
                                                    <textarea type="text" class="form-control" name="map" value="<?php echo e(old('map')); ?>"></textarea>
                                                </div>
                                                <?php $__errorArgs = ['map'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                	<em class="text-danger"><?php echo e($message); ?></em>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Amenity <em class="text-danger">*</em></label>
                                            <div id="output"></div>
                                            <select data-placeholder="Choose Amenity ..." name="amenity[]" multiple class="chosen-select" required="required">
                                                <?php $__currentLoopData = $amenity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php $selected = '' ?>
                                                <?php if(! empty(old('amenity'))): ?>
                                                    <?php if(in_array($item->id,old('amenity'))): ?>
                                                        <?php $selected = 'selected' ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                                    <option value="<?php echo e($item->id); ?>" <?php echo e($selected); ?>><?php echo e($item->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                            <?php $__errorArgs = ['amenity'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                            	<em class="text-danger"><?php echo e($message); ?></em>
                                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                        </div>
                                        <div class="col-md-6">
                                            
                                        </div>
                                    </div>
                                    <div class="card-header border-bottom mx--15 my-3">
                                        <h4 class="m-0 text-uppercase">Specification</h4>
                                    </div>
                                    <div class="form-row mt-5 " id="fetch_product_info">
                                        <div class="col-lg-6">
                                            <label>Specification <span class="text-danger">*</span> </label>
                                                <div class="form-group">
                                                    <select class="form-control" id="sp_name">
                                                        <option value="">Choose...</option>
                                                        <?php $__currentLoopData = $Specification; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($item['id']); ?>"><?php echo e($item['name']); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                    <?php $__errorArgs = ['sp_name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                    	<em class="text-danger"><?php echo e($message); ?></em>
                                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                </div>
                                            <span id="err_specification" class="text-danger"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                                <label>Description</label>
                                                <textarea type="text" class="form-control" id="sp_description"></textarea>
                                                <span id="err_description" class="text-danger"></span>
                                                <?php $__errorArgs = ['sp_description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                	<em class="text-danger"><?php echo e($message); ?></em>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="invisible">add</label>
                                                <input type="button" class="btn btn-primary d-block w-100 invoice_add" name="" id="addSpecification" value="Add">
                                            </div>
                                    </div>

                                    <div class="table-responsive mt-5">
                                        <table class="table page-table">
                                            <thead class="back thead-light">
                                                <tr>
                                                    <th scope="col">No</th>
                                                    <th scope="col">Specification Name</th>
                                                    <th scope="col" style="width: 50%;">Description</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="product_data">
                                            <?php if(! empty(old('name'))): ?>
                                            <?php
                                                $name = old('name');
                                                $spe_id = old('spe_id');
                                                $description = old('description');
                                            ?>
                                            <?php for($i=0; $i < count($name); $i++): ?>
                                                <?php $specificationRow++; ?>
                                                    <tr id="row_<?php echo e($i+1); ?>" class="new_row">
                                                        <th><?php echo e($i+1); ?></th>
                                                        <td>
                                                            <?php echo e($name[$i]); ?>

                                                            <input type="hidden" name="name[]" id="spe_name<?php echo e($i+1); ?>" value="<?php echo e($name[$i]); ?>">
                                                            <input type="hidden" value="<?php echo e($spe_id[$i]); ?>" id="spe_id<?php echo e($i+1); ?>" name="spe_id[]">
                                                        </td>
                                                        <td>
                                                            <?php echo e($description[$i]); ?>

                                                            <input type="hidden" id="spe_description<?php echo e($i+1); ?>" name="description[]" value="<?php echo e($description[$i]); ?>">
                                                        </td>
                                                        <td>
                                                            <span class="d-inline-block back_col view_details" tabindex="0" data-toggle="tooltip" title="Edit Specification" id="<?php echo e($i+1); ?>">
                                                                <a class="btn btn-primary p-2" style="color:white;">
                                                                    <!-- <i class="fa fa-pencil-alt" aria-hidden="true"></i> -->
                                                                    <svg class="svg-inline--fa fa-pencil-alt fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="pencil-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z"></path></svg><!-- <i class="fas fa-pencil-alt"></i> -->
                                                                </a>
                                                            </span>
                                                            <span class="d-inline-block back_col remove_details" tabindex="0" title="Delete Specification" id="<?php echo e($i+1); ?>">
                                                                <a class="btn btn-danger p-2" style="color:white;">
                                                                    <svg class="svg-inline--fa fa-trash fa-w-14" aria-hidden="true" focusable="false" data-prefix="fa" data-icon="trash" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16zM53.2 467a48 48 0 0 0 47.9 45h245.8a48 48 0 0 0 47.9-45L416 128H32z"></path></svg><!-- <i class="fa fa-trash" aria-hidden="true"></i> -->
                                                                </a>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                <?php endfor; ?>
                                            <?php endif; ?>
                                            <?php if(empty(old('name'))): ?>
                                            <?php if(! isEmptyArr($Specification->toArray())): ?>
                                                <?php $__currentLoopData = $Specification->toArray(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php $specificationRow++; ?>
                                                    <tr id="row_<?php echo e($k+1); ?>" class="new_row">
                                                        <th><?php echo e($k+1); ?></th>
                                                        <td>
                                                            <?php echo e($item['name']); ?>

                                                            <input type="hidden" name="name[]" id="spe_name<?php echo e($k+1); ?>" value="<?php echo e($item['name']); ?>">
                                                            <input type="hidden" value="<?php echo e($item['id']); ?>" id="spe_id<?php echo e($k+1); ?>" name="spe_id[]">
                                                        </td>
                                                        <td>
                                                            <?php echo $item['description']; ?>

                                                            <input type="hidden" id="spe_description<?php echo e($k+1); ?>" name="description[]" value="<?php echo e($item['description']); ?>">
                                                        </td>
                                                        <td>
                                                            <span class="d-inline-block back_col view_details" tabindex="0" data-toggle="tooltip" title="Edit Specification" id="<?php echo e($k+1); ?>">
                                                                <a class="btn btn-primary p-2" style="color:white;">
                                                                    <!-- <i class="fa fa-pencil-alt" aria-hidden="true"></i> -->
                                                                    <svg class="svg-inline--fa fa-pencil-alt fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="pencil-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z"></path></svg><!-- <i class="fas fa-pencil-alt"></i> -->
                                                                </a>
                                                            </span>
                                                            <span class="d-inline-block back_col remove_details" tabindex="0" title="Delete Specification" id="<?php echo e($k+1); ?>">
                                                                <a class="btn btn-danger p-2" style="color:white;">
                                                                    <svg class="svg-inline--fa fa-trash fa-w-14" aria-hidden="true" focusable="false" data-prefix="fa" data-icon="trash" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16zM53.2 467a48 48 0 0 0 47.9 45h245.8a48 48 0 0 0 47.9-45L416 128H32z"></path></svg><!-- <i class="fa fa-trash" aria-hidden="true"></i> -->
                                                                </a>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                            <?php endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="card-header border-bottom mx--15 my-3">
                                        <h4 class="m-0 text-uppercase">Flats</h4>
                                    </div>
                                     <div class="form-row mt-5 " id="fetch_product_info">
                                            <div class="col-lg-3 col-md-4 form-group">
                                                <label>Floor</label>
                                                <select class="form-control" id="floor">
                                                    <option value="">Choose...</option>
                                                    <option value="GF">GF</option>
                                                    <option value="I">I</option>
                                                    <option value="II">II</option>
                                                    <option value="III">III</option>
                                                    <option value="IV">IV</option>
                                                    <option value="V">V</option>
                                                    <option value="VI">VI</option>
                                                    <option value="VII">VII</option>
                                                    <option value="VIII">VIII</option>
                                                    <option value="IX">IX</option>
                                                    <option value="X">X</option>
                                                 </select>
                                                <span id="err_floor" class="text-danger"></span>
                                                <?php $__errorArgs = ['floor'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                <em class="text-danger"><?php echo e($message); ?></em>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                            <div class="col-lg-3 col-md-4 form-group">
                                                <label>Square Fit</label>
                                                <input type="text" class="form-control" id="sqft">
                                                <span id="err_sqft" class="text-danger"></span>
                                                <?php $__errorArgs = ['sqft'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                    <em class="text-danger"><?php echo e($message); ?></em>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                            <div class="col-lg-3 col-md-4 form-group">
                                                <label>BHK</label>
                                                <input type="text" class="form-control" id="bhk" onkeypress="return isNumber(event)">
                                                <span id="err_bhk" class="text-danger"></span>
                                                <?php $__errorArgs = ['bhk'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                    <em class="text-danger"><?php echo e($message); ?></em>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                            <div class="col-lg-2 col-md-4 col-6 form-group">
                                                <label>Status</label>
                                                <div class="form-group">
                                                    <select class="form-control" id="status">
                                                        <option value="">Choose...</option>
                                                        <option value="1">Sold</option>
                                                        <option value="2">Available</option>
                                                    </select>
                                                <?php $__errorArgs = ['status'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                    <em class="text-danger"><?php echo e($message); ?></em>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                                </div>
                                            </div>

                                            <div class="col-lg-1 col-md-4 col-6 form-group">
                                                <label class="invisible">add</label>
                                                <input type="button" class="btn btn-primary d-block w-100 invoice_add" name="" id="addflat" value="Add">
                                            </div>
                                    </div>
                                    <div class="table-responsive mt-5">
                                        <table class="table page-table">
                                            <thead class="back thead-light">
                                                <tr>
                                                    <th scope="col">No</th>
                                                    <th scope="col">Floor</th>
                                                    <th scope="col">Square Fit</th>
                                                    <th scope="col">BHK</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody class="text-center" id="flat_data">
                                            <?php if(! empty(old('floor'))): ?>
                                            <?php
                                                $floor = old('floor');
                                                $sqft = old('sqft');
                                                $bhk = old('bhk');
                                                $status = old('status');
                                            ?>
                                            <?php for($i=0; $i < count($floor); $i++): ?>
                                                <?php
                                                    $flat++;
                                                ?>
                                                <tr>
                                                    <th><?php echo e($i+1); ?></th>
                                                    <td>
                                                        <?php echo e($floor[$i]); ?>

                                                        <input type="hidden" name="floor[]" id="floor<?php echo e($i+1); ?>" value="<?php echo e($floor[$i]); ?>">
                                                    </td>
                                                    <td>
                                                        <?php echo e($sqft[$i]); ?>

                                                        <input type="hidden" id="sqft<?php echo e($i+1); ?>" name="sqft[]" value="<?php echo e($sqft[$i]); ?>">
                                                    </td>
                                                    <td>
                                                        <?php echo e($bhk[$i]); ?>

                                                        <input type="hidden" id="bhk<?php echo e($i+1); ?>" name="bhk[]" value="<?php echo e($bhk[$i]); ?>">
                                                    </td>
                                                    <td>
                                                        <?php echo e($status[$i]); ?>

                                                        <input type="hidden" id="status<?php echo e($i+1); ?>" name="status[]" value="<?php echo e($status[$i]); ?>">
                                                    </td>
                                                    <td>
                                                        <span class="d-inline-block back_col view_flat_details" tabindex="0" data-toggle="tooltip" title="Edit Flat" id="<?php echo e($i+1); ?>">
                                                            <a class="btn btn-primary p-2" style="color:white;">
                                                                <!-- <i class="fa fa-pencil-alt" aria-hidden="true"></i> -->
                                                                <svg class="svg-inline--fa fa-pencil-alt fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="pencil-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z"></path></svg><!-- <i class="fas fa-pencil-alt"></i> -->
                                                            </a>
                                                        </span>
                                                        <span class="d-inline-block back_col remove_flat_details" tabindex="0" title="Delete Flat" id="<?php echo e($i+1); ?>">
                                                            <a class="btn btn-danger p-2" style="color:white;">
                                                                <svg class="svg-inline--fa fa-trash fa-w-14" aria-hidden="true" focusable="false" data-prefix="fa" data-icon="trash" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16zM53.2 467a48 48 0 0 0 47.9 45h245.8a48 48 0 0 0 47.9-45L416 128H32z"></path></svg><!-- <i class="fa fa-trash" aria-hidden="true"></i> -->
                                                            </a>
                                                        </span>
                                                    </td>
                                                </tr>
                                            <?php endfor; ?>
                                            <?php endif; ?>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col mb-4 d-flex justify-content-end">
                                        <button type="submit" id="save_property" data-toggle="tooltip" title="All field is required" class="bg-success rounded text-white text-center py-2 px-3 d-inline-block border-0" style="box-shadow: inset 0 0 5px rgba(0,0,0,.2);">Save</button>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script src='https://harvesthq.github.io/chosen/chosen.jquery.js'></script>

<script>
    document.getElementById('output').innerHTML = location.search;
    $(".chosen-select").chosen();
    var base_url = '<?php echo e(url('/')); ?>';
    $(document).ready(function () {

    	$('[data-toggle="tooltip"]').tooltip();
    	
        $("#sp_name").change(function () {
            val = $(this).val();
            $.ajax({
                type: "get",
                url: base_url+"/ssb-admn/specification/getspecification/"+val,
                success: function (response) {
                    $('#sp_description').val(response.description);
                     CKEDITOR.instances.sp_description.setData( response.description );
                }
            });
        });

        var isShowHide = false;
        var count = <?php echo e($specificationRow); ?>;
        console.log(count);
        $('#addSpecification').click(function() {
			//set error message product/service and client
			var error_sp_name = '',
            sp_description = '',
            sp_name = $('#sp_name').children("option:selected").val();
			if (sp_name == '') {
				error_sp_name = 'Select at least one Specification';
				$('#err_specification').text(error_sp_name);
				$('#sp_name').css('border-color', '#cc0000');
			} else {
				error_sp_name = '';
				$('#err_specification').text(error_sp_name);
				$('#sp_name').css('border-color', '');
			}
			if (CKEDITOR.instances.sp_description.getData() == '') {
				sp_description = 'Enter Description';
				$('#err_description').text(sp_description);
				$('#sp_description').css('border-color', '#cc0000');
			} else {
				sp_description = '';
				$('#err_description').text(sp_description);
				$('#sp_description').css('border-color', '');
			}
			if (error_sp_name != '' || sp_description != '' )
				return false;
			else {
				var isHide = (!isShowHide) ? "d-none" : "";
				$("#empty_table").remove();

				var specification_name = $('#sp_name').children("option:selected").text(),
                    specification_val = $('#sp_name').children("option:selected").val(),
					specification_Desc = CKEDITOR.instances.sp_description.getData();

				count = (parseInt(count) + parseInt(1));
                output = '<tr id="row_' + count + '" class="new_row">';
				output += '<th>' + count + '</th>';
				output += '<td class="left">' + specification_name + ' <input type="hidden" name="name[]" id="spe_name' + count + '"  value="' + specification_name + '" /><input type="hidden" name="spe_id[]" id="spe_id' + count + '"  value="' + specification_val + '" /></td>';
				// output += '<td class="left">' + specification_Desc + ' <input type="hidden" name="description[]" id="spe_description' + count + '"  value="' + specification_Desc + '" /></td>';
                output += `<td class="left">${specification_Desc} <input type="hidden" name="description[]" id="spe_description${count}"  value='${specification_Desc}' /></td>`;
				output += '<td>';
				output += '<span class="d-inline-block back_col view_details" tabindex="0" data-toggle="tooltip" title="Edit Invoice" id="' + count + '">';
				output += '<a class="btn btn-primary p-2 mr-1">';
				output += '<i class="fa fa-pencil-alt" style="color:white;" aria-hidden="true"></i>';
				output += '</a>';
				output += '</span>';
				output += '<span class="d-inline-block back_col remove_details" tabindex="0" data-toggle="tooltip" title="Delete Invoice" id="' + count + '">';
				output += '<a class="btn btn-danger p-2">';
				output += '<i class="fa fa-trash" style="color:white;" aria-hidden="true"></i>';
				output += '</a>';
				output += '</span>';
				output += '</td>';
				output += '</tr>';
				$('#product_data').append(output);
                $("#sp_name").val(""), $("#sp_description").val("");
                CKEDITOR.instances.sp_description.updateElement();
                CKEDITOR.instances.sp_description.setData('');
				$("#save_property").removeClass("d-none");
			}
		});
        	//Display model popup selected row data
		$(document).on('click', '.view_details', function() {
			var row_id = $(this).attr("id");
			var sp_name = $("#spe_id" + row_id).val(),
				sp_desc = $("#spe_description" + row_id).val();
			console.log("sp_name"+sp_name);
            $('#edit_sp_name option[value='+sp_name+']').attr('selected','selected');
		    $('#edit_sp_name').val(sp_name);
			$('#edit_description').text(sp_desc);
			 CKEDITOR.instances.edit_description.setData( sp_desc);
			$('#edit_specification input[name="row_id"]').val(row_id);
			$('#edit_specification').modal('show');

		});
        //Edit Specific Row
		$(document).on('click', '#edit', function() {
			var isHide = (isShowHide) ? "d-none" : "";
			// 		var tax = $('#edit_tax_id').children("option:selected").val();

			var sp_name = $('#edit_sp_name').children("option:selected").text(),
                specification_val = $('#edit_sp_name').children("option:selected").val(),
                sp_description = CKEDITOR.instances.edit_description.getData();
            
            var row_id = $('#edit_specification input[name="row_id"]').val();
			output = '';
			output += '<th>' + row_id + '</th>';
			output += '<td class="left">' + sp_name + ' <input type="hidden" name="name[]" id="spe_name' + row_id + '" value="' + sp_name + '" /><input type="hidden" name="spe_id[]" id="spe_id' + row_id + '"  value="' + specification_val + '" /></td>';
			// output += '<td class="left">' + sp_description + ' <input type="hidden" name="description[]" id="spe_description' + row_id + '"  value="' + sp_description + '" /> </td>';
            output += `<td class="left">${sp_description} <input type="hidden" name="description[]" id="spe_description${row_id}" value='${sp_description}' /></td>`;
            output += '<td>';
			output += '<span class="d-inline-block back_col view_details" tabindex="0" data-toggle="tooltip" title="Edit Specification" id="' + row_id + '">';
			output += '<a class="btn btn-primary p-2 mr-1">';
			output += '<i class="fa fa-pencil-alt" style="color:white;" aria-hidden="true"></i>';
			output += '</a>';
			output += '</span>';
			output += '<span class="d-inline-block back_col remove_details" tabindex="0" data-toggle="tooltip" title="Delete Specification" id="' + row_id + '">';
			output += '<a class="btn btn-danger p-2">';
			output += '<i class="fa fa-trash"  style="color:white;" aria-hidden="true"></i>';
			output += '</a>';
			output += '</span>';
			output += '</td>';
			$('#row_' + row_id + '').html(output);
			$("#edit_specification .close").click();
            CKEDITOR.instances.sp_description.updateElement();
            CKEDITOR.instances.sp_description.setData('');
		});

        // Remove selected Row
		$(document).on('click', '.remove_details', function() {
			var row_id = $(this).attr("id");
			if (confirm("Are you sure you want to remove this Specification data?")) {
				$('#row_' + row_id + '').remove();
				if ($(".new_row").length <= 0) {
					count = 1;
					var row = '';
					row = '<tr id="empty_table">';
					row += '<td id="empty_table_colspan" colspan="4">You have not added any Specification yet.</td>';
					row += '</tr>';
					$('#product_data').append(row);
					$("#save_property").addClass("d-none");
				}
				count--;

			} else {
				return false;
			}
		});

        //flat

        var isShowHide = false;
        var fcount = <?php echo e($flat); ?>;
        $('#addflat').click(function() {
			//set error message product/service and client
			var error_floor = '',
                error_sqft= '',
                error_bhk = '',
                error_status = '';
                status = $('#status').children("option:selected").val();
			if ($('#floor').val()  == '') {
				error_floor = 'enter floor';
				$('#err_floor').text(error_floor);
				$('#floor').css('border-color', '#cc0000');
			} else {
				error_floor = '';
				$('#err_floor').text(error_floor);
				$('#floor').css('border-color', '');
			}
			if ($('#sqft').val() == '') {
				error_sqft = 'Enter Square Fit';
				$('#err_sqft').text(error_sqft);
				$('#sqft').css('border-color', '#cc0000');
			} else {
				error_sqft = '';
				$('#err_sqft').text(error_sqft);
				$('#sqft').css('border-color', '');
			}
            if ($('#bhk').val() == '') {
				error_bhk = 'Enter BHk';
				$('#err_bhk').text(error_bhk);
				$('#bhk').css('border-color', '#cc0000');
			} else {
				error_bhk = '';
				$('#err_bhk').text(error_bhk);
				$('#bhk').css('border-color', '');
			}
            if (status == '') {
				error_status = 'Enter Square Fit';
				$('#err_status').text(error_status);
				$('#status').css('border-color', '#cc0000');
			} else {
				error_status = '';
				$('#err_status').text(error_status);
				$('#status').css('border-color', '');
			}
			if (error_floor != '' || error_sqft != '' || error_bhk != '' || error_status != '')
				return false;
			else {
				var isHide = (!isShowHide) ? "d-none" : "";
				$("#empty_table").remove();

				var floor =  $('#floor').children("option:selected").val(),
                    sqft = $('#sqft').val(),
					bhk = $("#bhk").val(),
                    status_name = $('#status').children("option:selected").text(),
                    status = $('#status').children("option:selected").val();

				fcount = (parseInt(fcount) + parseInt(1));
                output = '<tr id="frow_' + fcount + '" class="new_row">';
				output += '<th>' + fcount + '</th>';
				output += '<td class="left">' + floor + ' <input type="hidden" name="floor[]" id="floor' + fcount + '"  value="' + floor + '" /></td>';
				output += '<td class="left">' + sqft + ' <input type="hidden" name="sqft[]" id="sqft' + fcount + '"  value="' + sqft + '" /></td>';
                output += '<td class="left">' + bhk + ' <input type="hidden" name="bhk[]" id="bhk' + fcount + '"  value="' + bhk + '" /></td>';
                output += '<td class="left">' + status_name + ' <input type="hidden" name="status[]" id="status' + fcount + '"  value="' + status + '" /></td>';
				output += '<td>';
				output += '<span class="d-inline-block back_col view_flat_details" tabindex="0" data-toggle="tooltip" title="Edit flat" id="' + fcount + '">';
				output += '<a class="btn btn-primary p-2 mr-1">';
				output += '<i class="fa fa-pencil-alt" style="color:white;" aria-hidden="true"></i>';
				output += '</a>';
				output += '</span>';
				output += '<span class="d-inline-block back_col remove_flat_details" tabindex="0" data-toggle="tooltip" title="Delete flat" id="' + fcount + '">';
				output += '<a class="btn btn-danger p-2">';
				output += '<i class="fa fa-trash" style="color:white;" aria-hidden="true"></i>';
				output += '</a>';
				output += '</span>';
				output += '</td>';
				output += '</tr>';
				$('#flat_data').append(output);
					$("#floor").val(""), $("#sqft").val(""),$("#bhk").val(""),$("#status").val("");

				$("#save_property").removeClass("d-none");

				
			}
		});
        	//Display model popup selected row data
		$(document).on('click', '.view_flat_details', function() {
            var row_id = $(this).attr("id");
			var floor =  $('#floor'+row_id).val(),
				sqft = $("#sqft" + row_id).val(),
                bhk = $("#bhk" + row_id).val(),
                status = $("#status" + row_id).val();

            $('#edit_status option[value='+status+']').attr('selected','selected');
            $('#edit_floor option[value='+floor+']').attr('selected','selected'),
		    $('#edit_sqft').val(sqft);
			$('#edit_bhk').val(bhk);
			$('#edit_flat input[name="row_id"]').val(row_id);
			$('#edit_flat').modal('show');

		});
        //Edit Specific Row for flat
		$(document).on('click', '#edit_flat_save', function() {

			// 		var tax = $('#edit_tax_id').children("option:selected").val();

			var  status_name = $('#edit_status').children("option:selected").text(),
                status = $('#edit_status').children("option:selected").val(),
				floor = $('#edit_floor').children("option:selected").val(),
                sqft = $("#edit_sqft").val(),
                bhk = $("#edit_bhk").val();


			var row_id = $('#edit_flat input[name="row_id"]').val();
			output = '';
			output += '<th>' + row_id + '</th>';
			output += '<td class="left">' + floor + ' <input type="hidden" name="floor[]" id="floor' + row_id + '" value="' + floor + '" /></td>';
			output += '<td class="left">' + sqft + ' <input type="hidden" name="sqft[]" id="sqft' + row_id + '"  value="' + sqft + '" /> </td>';
            output += '<td class="left">' + bhk + ' <input type="hidden" name="bhk[]" id="bhk' + row_id + '"  value="' + bhk + '" /> </td>';
            output += '<td class="left">' + status_name + ' <input type="hidden" name="status[]" id="status' + row_id + '"  value="' + status + '" /> </td>';
			output += '<td>';
			output += '<span class="d-inline-block back_col view_flat_details" tabindex="0" data-toggle="tooltip" title="Edit flat" id="' + row_id + '">';
			output += '<a class="btn btn-primary p-2 mr-1">';
			output += '<i class="fa fa-pencil-alt" style="color:white;" aria-hidden="true"></i>';
			output += '</a>';
			output += '</span>';
			output += '<span class="d-inline-block back_col remove_flat_details" tabindex="0" data-toggle="tooltip" title="Delete flat" id="' + row_id + '">';
			output += '<a class="btn btn-danger p-2">';
			output += '<i class="fa fa-trash"  style="color:white;" aria-hidden="true"></i>';
			output += '</a>';
			output += '</span>';
			output += '</td>';
			$('#frow_' + row_id + '').html(output);
			$("#edit_flat .close").click();

		});
         // Remove selected Row
		$(document).on('click', '.remove_flat_details', function() {
			var row_id = $(this).attr("id");
			if (confirm("Are you sure you want to remove this flat?")) {
				$('#frow_' + row_id + '').remove();
				if ($(".new_row").length <= 0) {
					fcount = 1;
					var row = '';
					row = '<tr id="empty_table">';
					row += '<td id="empty_table_colspan" colspan="13">You have not added any Flat yet.</td>';
					row += '</tr>';
					$('#flat_data').append(row);
					$("#save_property").addClass("d-none");
				}
				fcount--;

			} else {
				return false;
			}
		});
        $("#add_image").click(function (e) {
            var html = "";
            var img = $('#imageData').children('.image').length;
            html = `<div class="image">
                    <img src="<?php echo e(asset('public/images/no-image.png')); ?>" width="100"
                        height="100" id="artPrevImage_${img}" class="image"
                        style="margin-bottom:0px;padding:3px;" alt="logo" /><br />
                    <input type="file" name="image[]" id="ariImg_${img}"
                        onchange="readURL(this,'${img}');" style="display: none;"
                        accept="image/jpg,image/png">
                    <input type="hidden" value="" name="image[]" id="hiddenArtImgLogo" />
                    <a onclick="$('#ariImg_${img}').trigger('click');">Browse</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a style="clear:both;"
                        onclick="javascript:clear_image('artPrevImage_${img}')">Clear</a></div>`;
            $("#imageData").append(html);
            var img = $('#imageData div.image');
            var TotalImg = $('#imageData > div').length;
            if((TotalImg - 1)  % 6 === 0){
                $(img[TotalImg - 1]).addClass('ml-0');
            }

        });
    });

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/srisupra/web/srisuprabhathambuilder.com/public_html/resources/views/property/create.blade.php ENDPATH**/ ?>