@extends('layouts.front')
@section('css')
<link href="{{asset('public/front/css/lightbox.css')}}" rel="stylesheet" />
<style>
    /*jssor slider loading skin spin css*/
    .jssorl-009-spin img {
        animation-name: jssorl-009-spin;
        animation-duration: 1.6s;
        animation-iteration-count: infinite;
        animation-timing-function: linear;
    }

    @keyframes jssorl-009-spin {
        from { transform: rotate(0deg); }
        to { transform: rotate(360deg); }
    }

    /*jssor slider arrow skin 106 css*/
    .jssora106 {display:block;position:absolute;cursor:pointer;}
    .jssora106 .c {fill:#fff;opacity:0;}
    .jssora106 .a {fill:none;stroke:#000;stroke-width:350;stroke-miterlimit:10;}
    .jssora106:hover .c {opacity:.5;}
    .jssora106:hover .a {opacity:.8;}
    .jssora106.jssora106dn .c {opacity:.2;}
    .jssora106.jssora106dn .a {opacity:1;}
    .jssora106.jssora106ds {opacity:.3;pointer-events:none;}

    /*jssor slider thumbnail skin 101 css*/
    .jssort101 .p {position: absolute;top:0;left:0;box-sizing:border-box;background:#000;}
    .jssort101 .p .cv {position:relative;top:0;left:0;width:100%;height:100%;border:2px solid #000;box-sizing:border-box;z-index:1;}
    .jssort101 .a {fill:none;stroke:#fff;stroke-width:400;stroke-miterlimit:10;visibility:hidden;}
    .jssort101 .p:hover .cv, .jssort101 .p.pdn .cv {border:none;border-color:transparent;}
    .jssort101 .p:hover{padding:2px;}
    .jssort101 .p:hover .cv {background-color:rgba(0,0,0,6);opacity:.35;}
    .jssort101 .p:hover.pdn{padding:0;}
    .jssort101 .p:hover.pdn .cv {border:2px solid #fff;background:none;opacity:.35;}
    .jssort101 .pav .cv {border-color:#fff;opacity:.35;}
    .jssort101 .pav .a, .jssort101 .p:hover .a {visibility:visible;}
    .jssort101 .t {position:absolute;top:0;left:0;width:100%;height:100%;border:none;opacity:.6;}
    .jssort101 .pav .t, .jssort101 .p:hover .t{opacity:1;}
     #jssor_1{width: 100% !important;}



     .lightbox {
background-color: rgba(0, 0, 0, 0.8);
/*  overflow: scroll;*/
position: fixed;
display: none;
z-index: 10000000000;
bottom: 0;
right: 0;
left: 0;
top: 0;
}
.lightbox-container {
position: relative;
max-width: 960px;
margin: 5% auto;
display: block;
padding: 0 3%;
height: auto;
z-index: 10;
}
@media screen and (max-width: 768px) {
.lightbox-container {
margin-top: 10%;
}
}
@media screen and (max-width: 414px) {
.lightbox-container {
margin-top: 13%;
}
}
/*.lightbox-content {
box-shadow: 0 1px 6px rgba(0, 0, 0, 0.7);
}*/
.lightbox-close {
text-transform: uppercase;
background: transparent;
position: absolute;
font-weight: 300;
font-size: 12px;
display: block;
border: none;
color: white;
top: -22px;
right: 3%;
}

.video-container {
padding-bottom: 56.25%;
position: relative;
padding-top: 30px;
overflow: hidden;
height: 0;
}
.video-container iframe,
.video-container object,
.video-container embed {
position: absolute;
height: 100%;
width: 100%;
left: 0;
top: 0;
}
/* IGNORE FORM THIS POINT ON */
/*body {
background: #efefef;
}*/
#container {
/*border-radius: 4px;*/
/*max-width: 300px;*/
/*height: auto;*/
/*padding: 50px;*/
/*background: white;*/
/*margin: 100px auto;*/
}
#playme {
/*background: #007fed;*/
text-transform: uppercase;
font-weight: 300;
border: none;
color: white;
padding: 10px 15px;
display: inline-block;
font-size: 14px;
margin: 0;
}
</style>
@endsection
@section('content')
    
<!--=================================
Breadcrumb -->
<div class="bg-light">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="index.html"> <i class="fas fa-home"></i> </a></li>
  
            <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span> Gallery
            </span></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!--=================================
  Breadcrumb -->
  <div class="container mt-5">
      <div class="row">
      <div class="col-md-12">
          <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:940px;height:480px;overflow:hidden;visibility:hidden;">
                  <!-- Loading Screen -->
                  <!-- <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                      <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="{{asset('public/front/images/spin.svg')}}" />
                  </div> -->
                  <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb1_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb1_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb2_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb2_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb3_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb3_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb4_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb4_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb5_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb5_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb6_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb6_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb7_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb7_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb8_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb8_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb9_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb9_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb10_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb10_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb11_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb11_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb12_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb12_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb13_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb13_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb14_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb14_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb15_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb15_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb16_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb16_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb17_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb17_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb18_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb18_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb19_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb19_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb20_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb20_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb21_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb21_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb22_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb22_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb23_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb23_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb24_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb24_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb25_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb25_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb26_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb26_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb27_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb27_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb28_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb28_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb29_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb29_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb30_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb30_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb31_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb31_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb32_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb32_resize.png')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('public/front/images/ssb33_resize.png')}}" />
                          <img data-u="thumb" src="{{asset('public/front/images/ssb33_resize.png')}}" />
                      </div>

                  </div><a data-scale="0" href="https://www.jssor.com" style="display:none;position:absolute;">web animation</a>
                  <!-- Thumbnail Navigator -->
                  <div data-u="thumbnavigator" class="jssort101" style="position:absolute;left:0px;bottom:0px;width:980px;height:100px;background-color:#000;" data-autocenter="1" data-scale-bottom="0.75">
                      <div data-u="slides">
                          <div data-u="prototype" class="p" style="width:190px;height:90px;">
                              <div data-u="thumbnailtemplate" class="t"></div>
                              <svg viewbox="0 0 16000 16000" class="cv">
                                  <circle class="a" cx="8000" cy="8000" r="3238.1"></circle>
                                  <line class="a" x1="6190.5" y1="8000" x2="9809.5" y2="8000"></line>
                                  <line class="a" x1="8000" y1="9809.5" x2="8000" y2="6190.5"></line>
                              </svg>
                          </div>
                      </div>
                  </div>
                  <!-- Arrow Navigator -->
                  <div data-u="arrowleft" class="jssora106" style="width:55px;height:55px;top:162px;left:30px;" data-scale="0.75">
                      <button type="button" role="presentation" class="owl-prev"><i class="fas fa-chevron-left"></i></button>
  
                      <!-- <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                          <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                          <polyline class="a" points="7930.4,5495.7 5426.1,8000 7930.4,10504.3 "></polyline>
                          <line class="a" x1="10573.9" y1="8000" x2="5426.1" y2="8000"></line>
                      </svg> -->
                  </div>
                  <div data-u="arrowright" class="jssora106" style="width:55px;height:55px;top:162px;right:30px;" data-scale="0.75">
                      <!-- <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                          <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                          <polyline class="a" points="8069.6,5495.7 10573.9,8000 8069.6,10504.3 "></polyline>
                          <line class="a" x1="5426.1" y1="8000" x2="10573.9" y2="8000"></line>
                      </svg> -->
                      <button type="button" role="presentation" class="owl-next"><i class="fas fa-chevron-right"></i></button>
                  </div>
          </div>
  
  <!-- video section start -->
              <div class="col-md-12 box-shadow-specifiation mt-5 mb-5">
                  <div id="container">
  
  
                  <label class="video-label">Video</label>
                  <div class="property_video_wrapper">
                      <div id="property_video_wrapper_player"></div>
                      
  
  
                          <a id="playme" onclick="revealVideo('video','youtube')"  data-autoplay="true" data-vbtype="video" class="venobox vbox-item"><img src="http://bootstrapmonster.com/ssb/wp-content/uploads/2020/09/21st-street-1110x623.jpg" alt="video image"></a>
  
  
                          
                      </a>
                  </div>
  
                  <div id="video" class="lightbox" onclick="hideVideo('video','youtube')">
                    <div class="lightbox-container">  
                      <div class="lightbox-content">
                        
                        <button onclick="hideVideo('video','youtube')" class="lightbox-close">
                           ✕
                        </button>
                        <div class="video-container">
                          <iframe id="youtube" width="960" height="540" src="http://player.vimeo.com/video/17882714?autoplay=1" autoplay="1" allow="autoplay" frameborder="0" allowfullscreen></iframe>
                        </div>      
                        
                      </div>
                    </div>
                  </div>
              </div>
  <!-- video section end -->
  
  
      </div>
  
  
      </div>
      </div>
  </div>
          <!-- video section End -->
  
@endsection
@section('script')

<script type="text/javascript">
  
  // Function to reveal lightbox and adding YouTube autoplay
  function revealVideo(div,video_id) {
    var video = document.getElementById(video_id).src;
    document.getElementById(video_id).src = video+'&autoplay=1'; // adding autoplay to the URL
    document.getElementById(div).style.display = 'block';
  }
  
// Hiding the lightbox and removing YouTube autoplay
  function hideVideo(div,video_id) {
    var video = document.getElementById(video_id).src;
    var cleaned = video.replace('&autoplay=1',''); // removing autoplay form url
    document.getElementById(video_id).src = cleaned;
    document.getElementById(div).style.display = 'none';
  }
</script>

    <script>
          window.jssor_1_slider_init = function() {

var jssor_1_SlideshowTransitions = [
  {$Duration:800,x:0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
  {$Duration:800,x:-0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
  {$Duration:800,x:-0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
  {$Duration:800,x:0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
  {$Duration:800,y:0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
  {$Duration:800,y:-0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
  {$Duration:800,y:-0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
  {$Duration:800,y:0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
  {$Duration:800,x:0.3,$Cols:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
  {$Duration:800,x:0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
  {$Duration:800,y:0.3,$Rows:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
  {$Duration:800,y:0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
  {$Duration:800,y:0.3,$Cols:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
  {$Duration:800,y:-0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
  {$Duration:800,x:0.3,$Rows:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
  {$Duration:800,x:-0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
  {$Duration:800,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
  {$Duration:800,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$SlideOut:true,$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
  {$Duration:800,$Delay:20,$Clip:3,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
  {$Duration:800,$Delay:20,$Clip:3,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
  {$Duration:800,$Delay:20,$Clip:12,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
  {$Duration:800,$Delay:20,$Clip:12,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
];

var jssor_1_options = {
  $AutoPlay: 1,
  $SlideshowOptions: {
    $Class: $JssorSlideshowRunner$,
    $Transitions: jssor_1_SlideshowTransitions,
    $TransitionsOrder: 1
  },
  $ArrowNavigatorOptions: {
    $Class: $JssorArrowNavigator$
  },
  $ThumbnailNavigatorOptions: {
    $Class: $JssorThumbnailNavigator$,
    $SpacingX: 5,
    $SpacingY: 5
  }
};

var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

/*#region responsive code begin*/

var MAX_WIDTH = 10000; //980

function ScaleSlider() {
    var containerElement = jssor_1_slider.$Elmt.parentNode;
    var containerWidth = containerElement.clientWidth;

    if (containerWidth) {

        var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

        jssor_1_slider.$ScaleWidth(expectedWidth);
    }
    else {
        window.setTimeout(ScaleSlider, 30);
    }
}

ScaleSlider();

$Jssor$.$AddEvent(window, "load", ScaleSlider);
$Jssor$.$AddEvent(window, "resize", ScaleSlider);
$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
/*#endregion responsive code end*/
};

jssor_1_slider_init();

</script>
@endsection