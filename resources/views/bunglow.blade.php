@extends('layouts.front')
@section('content')
    
<!--=======breadcrumb -->
<div class="bg-light">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="index.html"> <i class="fas fa-home"></i> </a></li>
            
            <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span> Bunglow </span></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!--=====breadcrumb -->
  
  <section id="projects" class="projects section mt-5">
    <div class="container sri-builder">
      <h2 class="section-title text-center" style="font-size: 28px">Craftmanship From KH</h2>
    </div>


    <!-- <div class="section-content">
        <div class="projects-carousel js-projects-carousel js-projects-gallery">
          <?php
              for ($i=1; $i < 32; $i++) {
          ?>
          <div class="project project-light">
            <a href="{{asset('public/front/images/ssb'.$i.'_resize.png')}}" title="project 1">
              <figure style="height:500px;overflow: hidden;">
                <img src="{{asset('public/front/images/ssb'.$i.'_resize.png')}}" class="w-100 h-100"/>
                <div class="carousel-caption owl-section">
                    <h4>residance for mr. kumar</h4>
                    <span>architecture</span>
                </div>
              </figure>
            </a>
          </div>
          <?php } ?>
        </div>
    </div> -->

  <div class="layout mb-5">
      <div class="content">   
          <section id="projects" class="projects section">
            
            <div class="section-content">
              <div class="projects-carousel js-projects-carousel js-projects-gallery">  

                <div class="project">
                  <a href="javascript:void(0)" title="project 3" data-toggle="modal" data-target="#vivekanand">
                    <figure>
                    <figure>
                      <img alt="" src="{{asset('public/front/images/Vivek/vivek(1).jpg')}}">
                      <figcaption>
                        <h3 class="project-title">
                          Residence Of Mr. Vivek Anand
                        </h3>
                        <h4 class="project-category">
                          Architecture
                        </h4>
                        <div class="project-zoom"></div>
                      </figcaption>
                    </figure>
                  </a>
                </div>

                <div class="project project-light">
                  <a href="javascript:void(0)" title="project 1" data-toggle="modal" data-target="#kumar">
                    <figure>
                      <img src="{{asset('public/front/images/Kumar/kumar_ssb (8).jpg')}}">
                      <figcaption>
                        <h3 class="project-title">
                          Residence Of Mr. Kumar
                        </h3>
                        <h4 class="project-category">
                          Architecture
                        </h4>
                        <div class="project-zoom"></div>
                      </figcaption>
                    </figure>
                  </a>
                </div>

                <div class="project">
                  <a href="javascript:void(0)" title="project 3" data-toggle="modal" data-target="#ravichandran">
                    <figure>
                    <figure>
                      <img alt="" src="{{asset('public/front/images/Ravichandran/Ravichandran(1).JPG')}}" class="w-100">
                      <figcaption>
                        <h3 class="project-title">
                          Residence Of Mr. Ravichandran
                        </h3>
                        <h4 class="project-category">
                          Architecture
                        </h4>
                        <div class="project-zoom"></div>
                      </figcaption>
                    </figure>
                  </a>
                </div>


                <div class="project project-light">
                  <a href="javascript:void(0)" title="project 2" data-toggle="modal" data-target="#murali">
                    <figure>
                      <img alt="" src="{{asset('public/front/images/Murali/murali (1).JPG')}}">
                      <figcaption>
                        <h3 class="project-title">
                          Residence Of Mr. Murali
                        </h3>
                        <h4 class="project-category">Architecture</h4>
                        <div class="project-zoom"></div>
                      </figcaption>
                    </figure>
                  </a>
                </div>
              </div>
            </div>
          </section>
      </div>
  </div>




  <!-- Modal -->
  <div class="modal fade modal-back" id="vivekanand" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content modal-gallery">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        <div class="modal-body">

          <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              @for ($i = 1; $i <= 39; $i++)
              <div class="carousel-item {{($i == 1) ? 'active' : ''}}">
                <img class="d-block w-100" src="{{asset('public/front/images/Vivek/vivek('.$i.').jpg')}}" alt="First slide">
                <div class="slider-caption mt-2">
                  <h4 style="color: #969696;">Residence Of Mr. Vivek Anand</h4>
                </div>
              </div>
              @endfor
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade modal-back" id="murali" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content modal-gallery">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        <div class="modal-body">

          <div id="carouselmurali" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              @for ($i = 1; $i <= 13; $i++)
              <div class="carousel-item {{($i == 1) ? 'active' : ''}}">
                <img class="d-block w-100" src="{{asset('public/front/images/Murali/murali ('.$i.').JPG')}}" alt="First slide">
                <div class="slider-caption mt-2">
                  <h4 style="color: #969696;">Residence Of Mr. Murali</h4>
                </div>
              </div>
              @endfor
            </div>
            <a class="carousel-control-prev" href="#carouselmurali" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselmurali" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade modal-back" id="kumar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content modal-gallery">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        <div class="modal-body">

          <div id="carouselkumar" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              @for ($i = 1; $i <= 33; $i++)
              <div class="carousel-item {{($i == 1) ? 'active' : ''}}">
                <img class="d-block w-100" src="{{asset('public/front/images/Kumar/kumar_ssb ('.$i.').jpg')}}" alt="First slide">
                <div class="slider-caption mt-2">
                  <h4 style="color: #969696;"> Residence Of Mr. Kumar</h4>
                </div>
              </div>
              @endfor
            </div>
            <a class="carousel-control-prev" href="#carouselkumar" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselkumar" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade modal-back" id="ravichandran" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content modal-gallery">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        <div class="modal-body">

          <div id="carouselravichandran" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              @for ($i = 1; $i <= 14; $i++)
              <div class="carousel-item {{($i == 1) ? 'active' : ''}}">
                <img class="d-block w-100" src="{{asset('public/front/images/Ravichandran/Ravichandran('.$i.').JPG')}}" alt="First slide">
                <div class="slider-caption mt-2">
                  <h4 style="color: #969696;"> Residence Of Mr. Ravichandran</h4>
                </div>
              </div>
              @endfor
            </div>
            <a class="carousel-control-prev" href="#carouselravichandran" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselravichandran" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

  <!--=================================
  testimonial -->
  <section class="testimonial-main bg-holder" style="background-image: url({{asset('public/front/images/Kishore.jpg')}});">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <div>
            <div class="">
              <div class="testimonial">
                <div class="testimonial-content kishore-about">
                  <p>Our Concepts and plans are crafted by renowned designers and architects, to provide a dream home that's stylish with a modern layout to give our clients a luxurious Home for the best affordable price.</p>
                </div>
                <div class="testimonial-name">
                  <h6 class="text-primary mb-1">MR.KISHORE RAVICHANDRAN,</h6>
                  <span>Managing Director.,</span>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--=================================
  testimonial -->
  
  <section>
    <div class="container">
      <div class="row mt-5">
          <div class="col-md-6">
            <div class="about-detail">
              <p>I grew up watching my dad spend his life trying to satisfy his customers and people with the best he can in this business. Being from a small background he could build this business from nothing to a well-established group of companies. That’s when I realized when he could bring a huge success from very little, why can’t an MBA from London hike the business achieve a bigger triumph. I am looking at a different set of target customers. Mostly NRIs and foreigners who would want their homes to be of an international standard not only for a lifestyle purpose but also for the value for the amount of money they spend. I have learned to adapt to the situation & environment and that I have to be the best in what I do to be the best in the industry. Being able to create an upgraded level of lifestyle matching the latest trend for your home is my ultimate goal. Learning the business from dad is set off to open a group of Kishore Homes. Though I’m still learning every day with my father’s blessings and your good wishes.</p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="kishore-image">
              <div class="border-kishore-logo">
                <img src="{{asset('public/front/images/kishore-logo.png')}}" alt="k-logo" />
              </div>
            </div>
          </div>
      </div>
    </div>
  </section>

    
  <section class="my-5">
    <div class="container">
        <div class="sri-builder bunglow-first-pera">
            <h1 class="f-silver-south d-none" style="font-size: 34px">Kishore Homes</h1>
            <p>We at Kishore Homes, are the ultimate turnkey leader in Chennai. Your beautiful Dream home needs a lot of attention and the process to carry, trust us and we will hand over your beautiful dream home with lots of care and deliver it in a professional manner.
            Either if you own a land or looking for a land for construction in any specific area around Chennai, KH will source the perfect land of your requirement, construction in the span of time and provide the turnkey solutions with the professional experience.
            Based on the requirement and to provide the best Interior contemporary designs that suits, we do procurement from various countries like Italy, China, Indonesia, Greece, and other parts of Europe as well, which in need of customized and tailor procuring furniture’s for your unique enticing home. We have global expertise in procurement and also sourcing experience in various parts of INDIA as well.

            KH, with our expertise we provide In-house architecture team, customised interior designs, proper planning, procurement documentations, engineering and construction all of them under one roof.

            We have been exploring the most stylish and efficient ways to identify to provide the best dream home over the last two decades, and our profile shows who we are in the Turnkey industry. It is our zeal and we thrive to deliver the best and magical home of your dreams which are in line with the contemporary industrial standards by eliminating intermediatory agents and implementing the best of the construction industry into making your dream home come true.

            From KH, we assure by providing world-class luxurious & stylish home to our customers.</p>
            
            <p>A Dream house needs a lot of attention and the process to carry”, which is why, we at Kishore Homes, are the ultimate turnkey solutions provider in Chennai. We eliminate the lengthy procedures which would start from Planning, procurement, Engineering, construction, testing and commissioning, instead we provide all of them under one roof in the most unique way leading to a stylish luxurious living. We have been exploring the most efficient way to the ideal home over the last two decades. It is our zeal and we thrive to deliver the best and magical home of your dreams for the best deal which are in line with the contemporary industrial standards by eliminating intermediatory agents and implementing the best of construction industry into making your dream home come true. Now, it is simpler to get dream home of your life with the most affordable price available in the market. </p>
        </div>
    </div>
</section>
  
  <!--=================================
    Featured properties-->
    
      <!-- Projects -->

      <!-- Services -->
  {{-- <section class="space-pb mt-5">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="section-title text-center">
            <h2>All Property</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
        </div>
      </div>
      <div class="row">
          @foreach ($property as $item)
          @php
         
         if(strpos($item->image, '|') !== false){
                $img = explode('|',$item->image);
                $count = count($img);
                $image  = $img[0];
              } else{
                  $image = $item->image;
                  $count = 1;
              }
             
          @endphp
        <div class="col-sm-6 col-md-4">
          <div class="property-item">
            <div class="property-image bg-overlay-gradient-04">
              <img class="img-fluid" src="{{asset('public/front/images/property/grid/01.jpg')}}" alt="">
              <div class="property-lable">
                <span class="badge badge-md badge-primary">Bungalow</span>
                <span class="badge badge-md badge-info">Sale </span>
              </div>
              <span class="property-trending" title="trending"><i class="fas fa-bolt"></i></span>
              <!--<div class="property-agent">-->
              <!--  <div class="property-agent-image">-->
              <!--    <img class="img-fluid" src="{{asset('public/front/images/avatar/01.jpg')}}" alt="">-->
              <!--  </div>-->
              <!--  <div class="property-agent-info">-->
              <!--    <a class="property-agent-name" href="#">Alice Williams</a>-->
              <!--    <span class="d-block">Company Agent</span>-->
              <!--    <ul class="property-agent-contact list-unstyled">-->
              <!--      <li><a href="#"><i class="fas fa-mobile-alt"></i> </a></li>-->
              <!--      <li><a href="#"><i class="fas fa-envelope"></i> </a></li>-->
              <!--    </ul>-->
              <!--  </div>-->
              <!--</div>-->
              <div class="property-agent-popup">
                <a href="#"><i class="fas fa-camera"></i>{{$count}}</a>
              </div>
            </div>
            <div class="property-details">
              <div class="property-details-inner">
                <h5 class="property-title"><a href="property-detail.html">{{$item->name}} </a></h5>
                <span class="property-address"><i class="fas fa-map-marker-alt fa-xs"></i>{{$item->address}}</span>
                <!--<span class="property-agent-date"><i class="far fa-clock fa-md"></i>10 days ago</span>-->
                <div class="property-price">₹{{$item->amount}}</div>
                <ul class="property-info list-unstyled d-flex">
                  <li class="flex-fill property-bed"><i class="fas fa-bed"></i>Bed<span>{{$item->bed}}</span></li>
                  <li class="flex-fill property-bath"><i class="fas fa-bath"></i>Bath<span>{{$item->bath}}</span></li>
                  <li class="flex-fill property-m-sqft"><i class="far fa-square"></i>sqft<span>{{$item->sqft}}m</span></li>
                </ul>
              </div>
              <div class="property-btn">
                <a class="property-link" href="{{url('property-detail/'.$item->id)}}">See Details</a>
                <ul class="property-listing-actions list-unstyled mb-0">
                  <li class="property-compare"><a data-toggle="tooltip" data-placement="top" title="Compare" href="#"><i class="fas fa-exchange-alt"></i></a></li>
                  <li class="property-favourites"><a data-toggle="tooltip" data-placement="top" title="Favourite" href="#"><i class="far fa-heart"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        @endforeach
  
      </div>
    </div>
  </section> --}}
  
@endsection

@section('script')
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      xfbml            : true,
      version          : 'v9.0'
    });
  };

  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your Chat Plugin code -->
<div class="fb-customerchat"
  attribution=setup_tool
  page_id="105584477968961"
theme_color="#f64546"
logged_in_greeting="You can dream, create, design, and build the most wonderful place in the world."
logged_out_greeting="You can dream, create, design, and build the most wonderful place in the world.">
</div>

@endsection

